import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import nodemailer from "nodemailer"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

// tslint:disable-next-line:no-unsafe-any
const reportsUser: string = functions.config().gmail.reports.username
// tslint:disable-next-line:no-unsafe-any
const reportsPass: string = functions.config().gmail.reports.password
const reportsMailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: reportsUser,
    pass: reportsPass
  },
})
export const sendReportEmail = async (props: { to: string[], subject: string, html: string }) => {
  console.log("send report email")
  console.log(props.to)
  console.log(props.subject)
  console.log(props.html)
  const mailOptions = {
    from: '"Reports" <reports@helpdor.com>',
    to: props.to,
    subject: props.subject,
    html: props.html
  }
  try {
    await reportsMailTransport.sendMail(mailOptions)
    console.log(`Email sent to: ${props.to}`)
  } catch (e) {
    console.error("There was an error while sending the email:", e)
  }
}

// tslint:disable-next-line:no-unsafe-any
const supportUser: string = functions.config().gmail.support.username
// tslint:disable-next-line:no-unsafe-any
const supportPass: string = functions.config().gmail.support.password
const supportMailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
      user: supportUser,
      pass: supportPass
  },
})
export const sendSupportEmail = async (props: { to: string, subject: string, html: string }) => {
  console.log("send support email")
  console.log(props.to)
  console.log(props.subject)
  console.log(props.html)
  const mailOptions = {
    from: '"Helpdor Asiakaspalvelu" <support@helpdor.com>',
    to: props.to,
    subject: props.subject,
    html: props.html
  }
  try {
    await supportMailTransport.sendMail(mailOptions)
    console.log(`Email sent to: ${props.to}`)
  } catch (e) {
    console.error("There was an error while sending the email:", e)
  }
}
