/** EXPORT ALL FUNCTIONS
 *
 *   Loads all `./lib/db/__/_.functions.js` files
 *   Exports a cloud function matching the file name
 *   Deployed function will have a name like: "dbOfferRequestsOnCreate-createMockupOffer"
 *
 *   Based on this thread:
 *     https://github.com/firebase/functions-samples/issues/170
 */
/* tslint:disable */
const glob = require("glob")
const camelCase = require("camelcase")

const files = glob.sync("./**/**/*.func.js", { cwd: __dirname, ignore: "./node_modules/**" });
for (let f = 0, fl = files.length; f < fl; f++) {
  const file = files[f];
  // Strip off '.func.js'
  const functionName = camelCase(file.slice(0, -8).split("lib")[1].split("/").join("_"));
  exports[functionName] = require(file);
}
