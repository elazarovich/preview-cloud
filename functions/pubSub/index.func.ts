import moment from "moment-timezone"
import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { OFFER_REQUEST_STATUS, OFFER_STATUS } from "../db/Status"
import { REF_NAMES } from "../db/RefNames"
import {
  OfferRequestDB,
  OfferRequestDateQuestion,
  OfferDB,
} from "helpdor-types"
const { OFFER_REQUESTS, OFFERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const findOfferRequestDeadline = (offerRequest: OfferRequestDB) => {
  const dateQuestion = offerRequest.questions.find(
    question => question.type === "date",
  ) as OfferRequestDateQuestion | undefined

  if (!dateQuestion) {
    return undefined
  } else if (dateQuestion.suitableTimes) {
    const suitableTimes = Object.values(dateQuestion.suitableTimes)
    const last = suitableTimes.pop()
    return last ? last.endTime : undefined
  } else {
    return dateQuestion.preferredTime || undefined
  }
}

const setOfferRequestsExpired = async (db: admin.database.Database) => {
  const offerRequestsSnapshot = await db
    .ref(`${OFFER_REQUESTS}`)
    .orderByChild("status")
    .equalTo(OFFER_REQUEST_STATUS.OPEN)
    .once("value")

  const offerRequestsToSetExpiredKeys: string[] = []
  offerRequestsSnapshot.forEach(snapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offerRequest: OfferRequestDB = snapshot.val()
    const deadline = findOfferRequestDeadline(offerRequest)
    if (
      deadline &&
      moment().diff(moment(deadline, "x"), "minutes") > 30 &&
      snapshot.key
    ) {
      offerRequestsToSetExpiredKeys.push(snapshot.key)
    }
  })
  return Promise.all(
    offerRequestsToSetExpiredKeys.map(key =>
      db
        .ref(`${OFFER_REQUESTS}`)
        .child(key)
        .child("status")
        .set(OFFER_REQUEST_STATUS.EXPIRED),
    ),
  )
}

const setOffersCompleted = async (db: admin.database.Database) => {
  const offersSnapshot = await db
    .ref(`${OFFERS}`)
    .orderByChild("status")
    .equalTo(OFFER_STATUS.ACCEPTED)
    .once("value")

  const offersToSetCompletedKeys: string[] = []
  offersSnapshot.forEach(snapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = snapshot.val()
    if (
      offer.actualStartTime &&
      offer.actualCompletedTime &&
      moment().diff(moment(offer.actualCompletedTime, "x"), "minutes") > 30 &&
      snapshot.key
    ) {
      offersToSetCompletedKeys.push(snapshot.key)
    }
  })
  return Promise.all(
    offersToSetCompletedKeys.map(key =>
      db
        .ref(`${OFFERS}`)
        .child(key)
        .child("status")
        .set(OFFER_STATUS.COMPLETED),
    ),
  )
}

const notifyUpcomingJob = async (db: admin.database.Database) => {
  const offersSnapshot = await db
    .ref(`${OFFERS}`)
    .orderByChild("status")
    .equalTo(OFFER_STATUS.ACCEPTED)
    .once("value")
  const now = moment()
  const remindersSentKeys: string[] = []
  offersSnapshot.forEach(snapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = snapshot.val()
    if (offer.agreedStartTime && !offer.preHourReminderSent) {
      const endTime = moment(offer.agreedStartTime, "x")
      if (endTime.diff(now, "minutes") <= 60 && snapshot.key) {
        remindersSentKeys.push(snapshot.key)
      }
    }
  })
  return Promise.all(
    remindersSentKeys.map(key =>
      db
        .ref(`${OFFERS}`)
        .child(key)
        .child("preHourReminderSent")
        .set(true),
    ),
  )
}

// tslint:disable-next-line:variable-name
export const quarter_hourly_job = functions.pubsub
  .schedule("every 15 minutes")
  .onRun(async () => {
    const db = admin.app().database()
    await setOfferRequestsExpired(db)
    await setOffersCompleted(db)
    await notifyUpcomingJob(db)
  })

const notifyNextDaysJob = async (db: admin.database.Database) => {
  const offersSnapshot = await db
    .ref(`${OFFERS}`)
    .orderByChild("status")
    .equalTo(OFFER_STATUS.ACCEPTED)
    .once("value")
  const now = moment()
  const today = now.dayOfYear()

  const remindersSentKeys: string[] = []
  offersSnapshot.forEach(snapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = snapshot.val()
    if (offer.agreedStartTime && !offer.preDayReminderSent) {
      const endTime = moment(offer.agreedStartTime, "x")
      const dayOfYear = endTime.dayOfYear()
      if (dayOfYear - today === 1 && snapshot.key) {
        remindersSentKeys.push(snapshot.key)
      }
    }
  })
  return Promise.all(
    remindersSentKeys.map(key =>
      db
        .ref(`${OFFERS}`)
        .child(key)
        .child("preDayReminderSent")
        .set(true),
    ),
  )
}

// tslint:disable-next-line:variable-name
export const every_evening_job = functions.pubsub
  .schedule("every day 18:00")
  .timeZone("Europe/Helsinki")
  .onRun(async () => {
    const db = admin.app().database()
    return notifyNextDaysJob(db)
  })
