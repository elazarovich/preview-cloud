import * as admin from "firebase-admin"
import * as functions from "firebase-functions"
import { Twilio } from "twilio"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

// tslint:disable-next-line:no-unsafe-any
const account: string = functions.config().twilio.account
// tslint:disable-next-line:no-unsafe-any
const token: string = functions.config().twilio.token
// tslint:disable-next-line:no-unsafe-any
const phoneNumber: string = functions.config().twilio.phone

const generateCode = () => {
  return Math.floor(1000 + Math.random() * 9000).toString()
}

export function sendSMS(
  phone: string,
  message: string
) {
    
    const client = new Twilio(account, token)
    return client.messages.create({
      body: message,
      from: phoneNumber,
      to: phone,
    })
  }

export async function sendVerificationCode(phone: string)  {
  const code = generateCode()
  const msg = `Helpdor verification code: ${code}`
  await sendSMS(phone, msg)
  return {code, msg}
}
