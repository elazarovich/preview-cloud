module.exports = {
  semi: false,
  trailingComma: "all",
  tabWidth: 2
}
