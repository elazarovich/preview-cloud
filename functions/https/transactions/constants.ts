export const PAYMENT_STATUS = {
  PAID: "paid",
  CANCELLED: "cancelled",
  ERROR: "error",
  DELAYED: "delayed",
  FAILED: "failed",
}
