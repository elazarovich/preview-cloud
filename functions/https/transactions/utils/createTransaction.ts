import moment from "moment-timezone"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../../db/RefNames"
import { TransactionDB } from "helpdor-types"
import { getOffer } from "../../../db/offers"
import { getOfferRequest } from "../../../db/offerRequests"
import { normalizeOfferRequestQuestions } from "../../verifications/utils/normalizeOfferRequestUtils"
import {
  ListPickerQuestionNormalized,
  NumberInputQuestionNormalized,
} from "../../verifications/utils/normalizeOfferRequestTypes"

const { TRANSACTIONS } = REF_NAMES

// tslint:disable-next-line:no-any
const getCheckSum = (orderId: any) => {
  let checkSum = -1
  const multipliers = [7, 3, 1]
  let multiplierIndex = 0
  let sum = 0
  // tslint:disable-next-line:no-unsafe-any
  for (let i = orderId.length - 1; i >= 0; i -= 1) {
    // tslint:disable-next-line:no-unsafe-any
    const value = orderId[i] * multipliers[multiplierIndex % 3]
    sum += value
    multiplierIndex += 1
  }

  checkSum = 10 - (sum % 10)

  if (checkSum === 10) {
    checkSum = 0
  }
  return checkSum
}

const getReadableReferenceNumber = (referenceNumber: string) => {
  let readableReferenceNumber = ""
  for (let i = 0; i < referenceNumber.length; i += 1) {
    if (i % 4 === 0) {
      readableReferenceNumber += ` ${referenceNumber[i]}`
    } else {
      readableReferenceNumber += `${referenceNumber[i]}`
    }
  }
  return readableReferenceNumber
}

const createReferenceNumber = (orderId: number) => {
  // TODO: need to fix this
  const checkSum = getCheckSum(`${orderId}`)
  return parseInt(`${orderId}${checkSum}`, 10)
}

// Calculates the price and duration for transaction
const getTransactionValues = async (props: CreateTransactionProps) => {
  let transactionValues: {
    duration: number
    amount: number
  }
  if (props.isExtending) {
    // values for Offer extension
    const offer = await getOffer(props.databaseInstance, props.offerId)
    if (!offer.timeToExtend) {
      throw new Error(`Invalid offer for extending ${offer.id}`)
    }
    transactionValues = {
      amount: offer.pricePerHour * offer.timeToExtend,
      duration: offer.timeToExtend,
    }
  } else if (props.offerId) {
    // values for Offer
    const offer = await getOffer(props.databaseInstance, props.offerId)
    transactionValues = {
      amount: offer.original.pricePerHour * offer.original.purchaseDuration,
      duration: offer.original.purchaseDuration,
    }
  } else {
    // values for Prepaid OfferRequest
    const offerRequest = await getOfferRequest(
      props.databaseInstance,
      props.offerRequestId,
    )
    const normalizedQuestions = normalizeOfferRequestQuestions(offerRequest)
    const category = offerRequest.category
    const normalizedDuration = normalizedQuestions[
      `${category}/fixed_offer/yes/duration`
    ] as ListPickerQuestionNormalized
    const normalizedPrice = normalizedQuestions[
      `${category}/fixed_offer/yes/price`
    ] as NumberInputQuestionNormalized
    transactionValues = {
      amount: Number(normalizedPrice.value) * normalizedDuration.value,
      duration: normalizedDuration.value,
    }
  }
  return transactionValues
}

interface CreateTransactionProps {
  databaseInstance: admin.database.Database
  offerRequestId: string
  buyerInformation: TransactionDB["buyerInformation"]
  offerId?: string
  isExtending: boolean
}

const createTransaction = async (props: CreateTransactionProps) => {
  const transactionValues = await getTransactionValues(props)
  const orderId = moment().valueOf()
  const referenceNumber = createReferenceNumber(orderId)
  const newTransaction: Omit<TransactionDB, "createdAt"> = {
    orderId,
    reference: getReadableReferenceNumber(`${referenceNumber}`).trim(),
    buyerInformation: props.buyerInformation,
    offerId: props.offerId || null,
    offerRequestId: props.offerRequestId,
    ...transactionValues,
  }

  const transactionsRef = props.databaseInstance.ref(`/${TRANSACTIONS}`)
  const { key } = await transactionsRef.push({
    createdAt: admin.database.ServerValue.TIMESTAMP,
    ...newTransaction,
  })

  const savedTransaction = await transactionsRef.parent.once("value")

  const returnValues: {
    transactionId: string | undefined
    transaction: TransactionDB
    referenceNumber: number
  } = {
    transactionId: key,
    transaction: savedTransaction.val() as TransactionDB,
    referenceNumber,
  }

  return returnValues
}

export default createTransaction
