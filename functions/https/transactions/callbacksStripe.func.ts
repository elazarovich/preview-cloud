import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import Stripe from "stripe"
import { PAYMENT_STATUS } from "./constants"
import { REF_NAMES } from "../../db/RefNames"
import { OFFER_STATUS } from "../../db/Status"
import { OfferDB, TransactionStatus, TransactionDB } from "helpdor-types"

// tslint:disable-next-line:no-unsafe-any
const privateKey = functions.config().stripe.key

// tslint:disable-next-line:no-unsafe-any
const successWebHookSecret = functions.config().stripe.hooks.completed
const { OFFERS, TRANSACTIONS, USERS, OFFER_REQUESTS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const updateOfferStatus = async (
  databaseInstance: admin.database.Database,
  offerId: string,
) => {
  const offerRef = databaseInstance.ref(`/${OFFERS}/${offerId}`)
  const offerSnapshot = await offerRef.once("value")
  // tslint:disable-next-line:no-unsafe-any
  const offer: OfferDB = offerSnapshot.val()

  if (offer.status === OFFER_STATUS.OPEN) {
    await offerRef.update({ status: OFFER_STATUS.ACCEPTED })
  } else if (
    offer.status === OFFER_STATUS.ACCEPTED &&
    offer.timeToExtend &&
    offer.timeToExtend > 0
  ) {
    const newExtensions = offer.extensions || []
    newExtensions.push({
      purchaseDuration: offer.timeToExtend,
      pricePerHour: offer.pricePerHour,
      createdAt: admin.database.ServerValue.TIMESTAMP,
    })
    await offerRef.update({
      timeToExtend: 0,
      extensions: newExtensions,
    })
  }
}

const updateStatus = async (
  transactionStatus: TransactionStatus,
  transactionId: string,
) => {
  console.log("updateStatus")
  console.log({ transactionStatus, transactionId })
  try {
    const databaseInstance = admin.app().database()

    const transctionRef = databaseInstance.ref(
      `/${TRANSACTIONS}/${transactionId}`,
    )
    const transctionSnapshot = await transctionRef.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const transaction: TransactionDB = transctionSnapshot.val()
    await transctionRef.update({ status: transactionStatus })

    console.log(`${transactionId}: PAYMENT STATUS === ${transactionStatus}`)

    if (transactionStatus === PAYMENT_STATUS.PAID) {
      if (transaction.offerId) {
        await updateOfferStatus(databaseInstance, transaction.offerId)
      } else {
        const offerRequestRef = await databaseInstance
          .ref(`/${OFFER_REQUESTS}`)
          .child(transaction.offerRequestId)
        await offerRequestRef.update({ prepaid: "paid" })
      }
      await databaseInstance
        .ref(`/${USERS}/${transaction.buyerInformation.buyerId}`)
        .child(TRANSACTIONS)
        .child(transactionId)
        .set(true)
    }
  } catch (exception) {
    console.log(`${transactionId}: PAYMENT STATUS === ${PAYMENT_STATUS.FAILED}`)
    console.error(exception)
    throw new Error(`Status update failed for transaction: ${transactionId}`)
  }
}

export const successWebHook = functions.https.onRequest(async (req, res) => {
  const signature = req.headers["stripe-signature"]
  if (!signature) {
    console.error(`Stripe success-hook missing signature: ${signature}`)
    return
  }
  try {
    const event = new Stripe(privateKey).webhooks.constructEvent(
      req.rawBody,
      signature,
      successWebHookSecret,
    )
    if (event.type === "checkout.session.completed") {
      const session = event.data.object
      await updateStatus(
        "paid",
        (session as any).client_reference_id, // tslint:disable-line
      )
    } else {
      console.error(`Unhandled Stripe even ${event.type}`)
      res.status(400).end()
    }
  } catch (err) {
    console.error(err)
    res.status(400).end()
  }
  res.json({ received: true })
})
