import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import _ from "lodash"
import Stripe from "stripe"
import createTransaction from "./utils/createTransaction"
import { REF_NAMES } from "../../db/RefNames"
import { OFFER_REQUEST_STATUS, OFFER_STATUS } from "../../db/Status"
import { getOfferRequest } from "../../db/offerRequests"
import { TransactionDB, OfferDB, OfferRequestDB } from "helpdor-types"
import { getUser } from "../../db/users"
import { getAvailableLocale, Locales } from "../../db/Translation"
import { getOffer, Offer } from "../../db/offers"

// tslint:disable-next-line:no-unsafe-any
const privateKey = functions.config().stripe.key as string
// tslint:disable-next-line:no-unsafe-any
const hostingUrl = functions.config().hosting.url as string

const { OFFERS, OFFER_REQUESTS, TRANSACTIONS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

interface StripeProps {
  secretKey: string
  amount: number
  name: string
  duration: number
  transactionId: string
  successUrl: string
  cancelUrl: string
  locale?: Locales
  email?: string
}

const getStripeSession = ({
  secretKey,
  amount,
  name,
  duration,
  transactionId,
  successUrl,
  cancelUrl,
  locale,
  email,
}: StripeProps) => {
  console.log("getStripeSession")
  console.log({ amount, name, duration, transactionId })
  console.log(secretKey.length)
  const stripe = new Stripe(secretKey)

  return new Promise((resolve, reject) => {
    return stripe.checkout.sessions.create(
      {
        locale,
        client_reference_id: transactionId,
        success_url: `https://${hostingUrl}/success/stripe?${encodeURIComponent(
          successUrl,
        )}`,
        cancel_url: `https://${hostingUrl}/cancel/stripe?${encodeURIComponent(
          cancelUrl,
        )}`,
        payment_method_types: ["card"],
        line_items: [
          {
            amount,
            quantity: 1,
            name,
            currency: "eur",
            description: `${name} - ${duration}h`,
          },
        ],
        customer_email: email,
      },
      (err, session) => {
        if (err) {
          reject(err)
        } else {
          resolve(session.id)
        }
      },
    )
  })
}

interface Body {
  buyerInformation: TransactionDB["buyerInformation"]
  categoryName: string
  offerId: string
  offerRequestId: string
  isExtending: boolean
  successUrl: string
  cancelUrl: string
}

// tslint:disable-next-line:no-any
const getValidatedBody = (body: any): Body | undefined => {
  if (body && typeof body === "object") {
    const data = body as Partial<Body>
    const {
      cancelUrl,
      successUrl,
      offerId,
      offerRequestId,
      categoryName,
      buyerInformation,
    } = data
    if (
      cancelUrl &&
      successUrl &&
      offerId &&
      offerRequestId &&
      categoryName &&
      buyerInformation &&
      buyerInformation.buyerId &&
      buyerInformation.email &&
      buyerInformation.name &&
      buyerInformation.userLocale
    ) {
      return {
        cancelUrl,
        successUrl,
        offerId,
        offerRequestId,
        categoryName,
        buyerInformation,
        isExtending: data.isExtending ? true : false,
      }
    }
  }
  return undefined
}

export const get = functions.https.onRequest(async (req, res) => {
  const { method, body, headers } = req
  const contentType = _.get(headers, "content-type")
  if (method === "POST" && _.startsWith(contentType, "application/json")) {
    try {
      const validatedBody = getValidatedBody(body)
      if (!validatedBody) {
        throw new functions.https.HttpsError(
          "invalid-argument",
          "Invalid body",
          validatedBody,
        )
      }

      const databaseInstance = admin.app().database()

      const offerRequest = await getOfferRequest(
        databaseInstance,
        validatedBody.offerRequestId,
      )

      if (!offerRequest) {
        throw new functions.https.HttpsError(
          "invalid-argument",
          "OfferRequest not found",
          `Trying to pay when OfferRequest (${validatedBody.offerRequestId}) can not be found`,
        )
      }

      const offerSnapshot = await databaseInstance
        .ref(`/${OFFERS}`)
        .child(validatedBody.offerId)
        .once("value")

      // tslint:disable-next-line:no-unsafe-any
      const offer: OfferDB = offerSnapshot.val()

      const isOpen =
        offerRequest.status === OFFER_REQUEST_STATUS.OPEN &&
        offer.status === OFFER_STATUS.OPEN

      if (isOpen || (validatedBody.isExtending && offer.timeToExtend)) {
        const { transactionId, transaction } = await createTransaction({
          databaseInstance,
          offerRequestId: validatedBody.offerRequestId,
          buyerInformation: validatedBody.buyerInformation,
          offerId: validatedBody.offerId,
          isExtending: validatedBody.isExtending,
        })
        if (!transactionId) {
          throw new functions.https.HttpsError(
            "cancelled",
            "Transaction could not be created",
            `Transaction could not be created for offerRequest (${validatedBody.offerId})`,
          )
        }
        const sessionId = await getStripeSession({
          amount: transaction.amount * 100,
          name: validatedBody.categoryName,
          duration: transaction.duration,
          transactionId,
          secretKey: privateKey,
          successUrl: validatedBody.successUrl,
          cancelUrl: validatedBody.cancelUrl,
          email: validatedBody.buyerInformation.email,
        })
        res.statusCode = 200
        return res.json({ sessionId, transactionId })
      }
      console.error(
        new Error(
          `Trying to pay when OfferRequest (${validatedBody.offerRequestId}) or Offer (${validatedBody.offerId}) status was ${offer.status}`,
        ),
      )
      return res.status(204).send("ok")
    } catch (exception) {
      console.error(exception)
      return res.status(500).send("ok")
    }
  }
  console.error(new Error("Invalid request"))
  return res.status(501).send("ok")
})

interface PrepayStripeSessionRequestProps {
  offerRequestId: string
  transactionId: string
}

const getPrepayStripeSession = async (
  req: functions.https.Request,
  res: functions.Response,
  secretKey: string,
) => {
  const {
    offerRequestId,
    transactionId,
  } = req.query as PrepayStripeSessionRequestProps

  const databaseInstance = admin.app().database()
  const offerRequestRef = await databaseInstance
    .ref(`/${OFFER_REQUESTS}/${offerRequestId}`)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const offerRequest: OfferRequestDB = offerRequestRef.val()
  if (!offerRequest) {
    res.statusCode = 404
    res.send({ message: "request not found" })
    return null
  }

  let offer: Offer
  if (offerRequest.prepaid === "paid") {
    if (offerRequest.offers) {
      const [offerId] = Object.keys(offerRequest.offers)
      offer = await getOffer(databaseInstance, offerId)
      if (!offer.timeToExtend) {
        res.statusCode = 404
        res.send({ message: "payment already completed" })
        return null
      }
    } else {
      res.statusCode = 404
      res.send({ message: "payment already completed" })
      return null
    }
  }

  const transactionRef = databaseInstance.ref(
    `/${TRANSACTIONS}/${transactionId}`,
  )
  const transactionSnapshot = await transactionRef.once("value")
  // tslint:disable-next-line:no-unsafe-any
  const transaction: TransactionDB = transactionSnapshot.val()
  if (!transaction) {
    res.statusCode = 404
    res.send({ message: "transaction not found" })
    return null
  }

  const buyer = await getUser(databaseInstance, offerRequest.buyerProfile)
  const sessionId = await getStripeSession({
    transactionId,
    secretKey,
    amount: transaction.amount * 100,
    duration: transaction.duration,
    name: offerRequest.category,
    successUrl: "/success/stripe",
    cancelUrl: "/success/canceled",
    locale: getAvailableLocale(buyer ? buyer.language : ""),
    email: buyer.email,
  })

  res.statusCode = 200
  return res.json({ sessionId, transactionId })
}

export const getPrepay = functions.https.onRequest(async (req, res) => {
  return getPrepayStripeSession(req, res, privateKey)
})
