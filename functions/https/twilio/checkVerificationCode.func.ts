import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { UserDB, ConfirmationCodeDB } from "helpdor-types"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const checkVerificationCode = async (
  req: functions.https.Request,
  res: functions.Response,
) => {
  // tslint:disable-next-line:no-unsafe-any
  const { uid, code } = req.query

  if (!uid || !code) {
    res.statusCode = 400
    res.send("missing uid or code")
    return
  }

  const databaseInstance = admin.app().database()

  const codeRef = await databaseInstance
    .ref(`/${REF_NAMES.CONFIRMATION_CODES}/${uid}`)
    .once("value")
  // tslint:disable-next-line:no-unsafe-any
  const codeValue: ConfirmationCodeDB = codeRef.val()
  if (!codeValue) {
    res.statusCode = 404
    res.send("This user has no confirmation codes associated with him")
    return
  }

  const userRef = await databaseInstance
    .ref(`/${REF_NAMES.USERS}/${uid}`)
    .once("value")
  // tslint:disable-next-line:no-unsafe-any
  const user: UserDB = userRef.val()
  if (!user || !userRef.key) {
    res.statusCode = 404
    res.send("User not found")
    return
  }

  if (codeValue.code === code) {
    const updatedUserData: Partial<UserDB> = {
      phone: user.phone,
    }
    await codeRef.ref.remove()
    await userRef.ref.child("phoneVerified").set(true)
    await admin.auth().updateUser(userRef.key, updatedUserData)
    res.statusCode = 200
    res.send("ok")
  } else {
    console.log(
      `Invalid code: ${code}, expected ${codeValue.code} for user ${userRef.key}`,
    )
    res.statusCode = 400
    res.send("Invalid code")
  }
}

export const check = functions.https.onRequest(async (req, res) => {
  return checkVerificationCode(req, res)
})
