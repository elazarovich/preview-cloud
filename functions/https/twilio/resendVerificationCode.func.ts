import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { sendVerificationCode } from "../../sms"
import { UserDB, ConfirmationCodeDB } from "helpdor-types"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const resendVerificationCode = async (
  req: functions.https.Request,
  res: functions.Response,
) => {
  // tslint:disable-next-line:no-unsafe-any
  const { uid }: { uid?: string } = req.query

  if (!uid) {
    res.statusCode = 400
    res.send("missing uid")
    return
  }

  const databaseInstance = admin.app().database()

  const userRef = await databaseInstance
    .ref(`/${REF_NAMES.USERS}/${uid}`)
    .once("value")
  // tslint:disable-next-line:no-unsafe-any
  const user: UserDB = userRef.val()
  if (!user) {
    res.statusCode = 404
    res.send("user not found")
    return
  }

  if (!user.phone) {
    res.statusCode = 400
    res.send("user phone not set")
    return
  }

  const { msg, code } = await sendVerificationCode(user.phone)

  const confirmationCodeDB: ConfirmationCodeDB = {
    code,
    createdAt: admin.database.ServerValue.TIMESTAMP,
  }
  void admin
    .app()
    .database()
    .ref(REF_NAMES.CONFIRMATION_CODES)
    .child(uid)
    .set(confirmationCodeDB)

  console.log(`Confirmation code re-sent (${msg} to ${user.phone})`)
  res.statusCode = 200
  res.send("ok")
}

export const resend = functions.https.onRequest(async (req, res) => {
  return resendVerificationCode(req, res)
})
