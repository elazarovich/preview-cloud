import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { APPROVED_STATUS } from "../../db/Status"
import { OfferRequestDB, OfferDB } from "helpdor-types"
import { getName } from "../../db/categories"
import {
  getNormalizedAddress,
  normalizeOfferRequestQuestions,
} from "./utils/normalizeOfferRequestUtils"
import {
  DateQuestionNormalized,
  ListPickerQuestionNormalized,
  NormalizedQuestion,
  NumberInputQuestionNormalized,
} from "./utils/normalizeOfferRequestTypes"
import { getAvailableLocale, Translate } from "../../db/Translation"
import { getUser } from "../../db/users"
import moment from "moment-timezone"
import { defaultTimeZone } from "../../locale"

const { IS_APPROVED } = APPROVED_STATUS
const { OFFER_REQUESTS, OFFERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const confirm = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-unsafe-any
  const { reqId }: { reqId?: string } = req.query

  if (!reqId) {
    res.statusCode = 400
    res.send("missing params")
    return
  }

  const databaseInstance = admin.app().database()
  const offerRequestRef = await databaseInstance
    .ref(`/${OFFER_REQUESTS}/${reqId}`)
    .once("value")
  // tslint:disable-next-line:no-unsafe-any
  const offerRequest: OfferRequestDB = offerRequestRef.val()

  if (!offerRequest) {
    res.statusCode = 404
    res.send("request not found")
    return
  }

  await databaseInstance
    .ref(`/${OFFER_REQUESTS}/${reqId}`)
    .child(IS_APPROVED)
    .set(true)

  res.statusCode = 200
  res.send("offer-request confirmed")
})

interface OfferDetails {
  date?: string
  time?: string
  duration?: string
  price?: string
}

function getNormalizedQuestionsDetails(
  normalizedPrefix: string,
  normalizedQuestionMap: { [id: string]: NormalizedQuestion },
): OfferDetails {
  const result: OfferDetails = {
    date: undefined,
    duration: undefined,
    price: undefined,
  }
  const normalizedDate = normalizedQuestionMap[
    `${normalizedPrefix}/when`
  ] as DateQuestionNormalized
  if (normalizedDate && normalizedDate.value) {
    const date = moment.utc(normalizedDate.value).tz(defaultTimeZone)
    result.date = date.format("DD.MM.YYYY")
    result.time = date.format("HH:mm")
  }
  const normalizedDuration = normalizedQuestionMap[
    `${normalizedPrefix}/fixed_offer/yes/duration`
  ] as ListPickerQuestionNormalized
  if (normalizedDuration && normalizedDuration.value) {
    result.duration = `${normalizedDuration.value}h`
  }
  const normalizedPrice = normalizedQuestionMap[
    `${normalizedPrefix}/fixed_offer/yes/price`
  ] as NumberInputQuestionNormalized
  if (normalizedPrice && normalizedPrice.value) {
    result.price = `${normalizedPrice.value}€/h`
  }
  return result
}

function getExtendedOfferDetails(
  offerRequest: OfferRequestDB,
  offer: OfferDB,
): OfferDetails {
  const actualStart = moment
    .utc(offer.actualStartTime)
    .tz(defaultTimeZone)
    .add(offerRequest.fixedDuration, "h")
  offer?.extensions?.forEach(extension => {
    actualStart.add(extension.purchaseDuration, "h")
  })

  return {
    time: actualStart.format("HH:mm"),
    duration: offer.timeToExtend ? `${offer.timeToExtend}h` : undefined,
    date: actualStart.format("DD.MM.YYYY"),
    price: offer.pricePerHour ? `${offer.pricePerHour}€/h` : undefined,
  }
}

interface OfferRequestDetailsRequest {
  offerRequestId: string
  offerId?: string
}

interface OfferRequestDetailsResponse {
  categoryName: string
  address: string | { from: string; to: string }
  translations: { [field: string]: string }
}

export const getDetails = functions.https.onRequest(async (req, res) => {
  const { offerRequestId, offerId } = req.query as OfferRequestDetailsRequest
  const databaseInstance = admin.app().database()
  const offerRequestRef = await databaseInstance
    .ref(`/${OFFER_REQUESTS}/${offerRequestId}`)
    .once("value")

  const offerRequest: OfferRequestDB = offerRequestRef.val()
  if (!offerRequest) {
    res.statusCode = 404
    res.send({ message: "offer request not found" })
    return
  }

  let details: OfferDetails
  if (offerId) {
    const offerRef = await databaseInstance
      .ref(`/${OFFERS}/${offerId}`)
      .once("value")

    const offer: OfferDB = offerRef.val()
    if (!offer) {
      res.statusCode = 404
      res.send({ message: `offer not found : ${JSON.stringify(offerId)}` })
      return
    }
    details = getExtendedOfferDetails(offerRequest, offer)
  } else {
    const normalizedQuestions = normalizeOfferRequestQuestions(offerRequest)
    details = getNormalizedQuestionsDetails(
      offerRequest.category,
      normalizedQuestions,
    )
  }

  const categoryName = await getName(databaseInstance, offerRequest.category)
  const buyer = await getUser(databaseInstance, offerRequest.buyerProfile)
  const locale = getAvailableLocale(buyer ? buyer.language : "")

  const response: OfferRequestDetailsResponse = {
    categoryName: categoryName[locale],
    address: getNormalizedAddress(offerRequest),
    ...details,
    translations: {
      pay: Translate(locale, "pay"),
      categoryName: Translate(locale, "service"),
      duration: Translate(locale, "duration"),
      price: Translate(locale, "total_price"),
      date: Translate(locale, "date"),
      time: Translate(locale, "time"),
      address: Translate(locale, "address"),
      from: Translate(locale, "from"),
      to: Translate(locale, "to"),
    },
  }

  res.statusCode = 200
  res.json(response)
})
