import { Label } from "helpdor-types"

export interface BaseNormalized {
  id: string
  name: Label
}

export interface TextAreaQuestionNormalized extends BaseNormalized {
  type: "textarea"
  value: string
}

export interface DateQuestionNormalized extends BaseNormalized {
  type: "date"
  value: number
}

export interface NumberQuestionNormalized extends BaseNormalized {
  type: "number"
  value: number
}

export interface NumberInputQuestionNormalized extends BaseNormalized {
  type: "number_input"
  value: string
}

export interface ListPickerQuestionNormalized extends BaseNormalized {
  type: "list"
  value: number
}

export interface RadioQuestionOptionNormalized {
  id: string
  name: Label
}

export interface RadioQuestionNormalized extends BaseNormalized {
  type: "radio"
  value: string
  options: RadioQuestionOptionNormalized[]
}

export interface CheckboxQuestionOptionNormalized {
  id: string
  name: Label
  checked: boolean
}

export interface CheckboxQuestionNormalized extends BaseNormalized {
  type: "checkbox"
  options: CheckboxQuestionOptionNormalized[]
}

export type NormalizedQuestion =
  | TextAreaQuestionNormalized
  | DateQuestionNormalized
  | NumberQuestionNormalized
  | NumberInputQuestionNormalized
  | ListPickerQuestionNormalized
  | RadioQuestionNormalized
  | CheckboxQuestionNormalized
