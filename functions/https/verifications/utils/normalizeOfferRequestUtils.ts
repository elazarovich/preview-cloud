import {
  Address,
  OfferRequestDB,
  OfferRequestCheckBoxQuestion,
  OfferRequestDateQuestion,
  OfferRequestListPicker,
  OfferRequestNumberInputQuestion,
  OfferRequestNumberQuestion,
  OfferRequestQuestionOption,
  OfferRequestRadioQuestion,
  OfferRequestTextAreaQuestion,
  OfferRequestQuestion,
} from "helpdor-types"
import {
  BaseNormalized,
  DateQuestionNormalized,
  ListPickerQuestionNormalized,
  NormalizedQuestion,
  NumberInputQuestionNormalized,
  NumberQuestionNormalized,
  TextAreaQuestionNormalized,
} from "./normalizeOfferRequestTypes"

const normalizeBaseQuestionData = (
  question: OfferRequestQuestion,
): BaseNormalized => ({
  id: question.id,
  name: question.name,
})

function normalizeTextAreaQuestion(
  question: OfferRequestTextAreaQuestion,
): TextAreaQuestionNormalized {
  return {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.value,
  }
}

function normalizeDateQuestion(
  question: OfferRequestDateQuestion,
): DateQuestionNormalized {
  return {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.preferredTime,
  }
}

function normalizeNumberQuestion(
  question: OfferRequestNumberQuestion,
): NumberQuestionNormalized {
  return {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.value,
  }
}

function normalizeNumberInputQuestion(
  question: OfferRequestNumberInputQuestion,
): NumberInputQuestionNormalized {
  return {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.value,
  }
}

function normalizeListPickerQuestion(
  question: OfferRequestListPicker,
): ListPickerQuestionNormalized {
  return {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.value,
  }
}

function normalizeNestedOptions(
  options: OfferRequestQuestionOption[],
): NormalizedQuestion[] {
  let result: NormalizedQuestion[] = []
  options.forEach(option => {
    if (!option.questions) {
      return
    }
    option.questions.forEach(optionQuestion => {
      const normalizedQuestion = normalizeOfferRequestQuestion(optionQuestion)
      if (Array.isArray(normalizedQuestion)) {
        result = [...result, ...normalizedQuestion]
      } else {
        result.push(normalizedQuestion)
      }
    })
  })
  return result
}

function normalizeRadioQuestion(
  question: OfferRequestRadioQuestion,
): NormalizedQuestion[] {
  const radioData = {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    value: question.selectedOption,
    options: question.options.map(option => ({
      id: option.id,
      name: option.name,
    })),
  }
  return [radioData, ...normalizeNestedOptions(question.options)]
}

function normalizeCheckBoxQuestion(
  question: OfferRequestCheckBoxQuestion,
): NormalizedQuestion[] {
  const radioData = {
    ...normalizeBaseQuestionData(question),
    type: question.type,
    options: question.options.map(option => ({
      id: option.id,
      name: option.name,
      checked: option.checked,
    })),
  }
  return [radioData, ...normalizeNestedOptions(question.options)]
}

function normalizeOfferRequestQuestion(
  offerRequestQuestion: OfferRequestQuestion,
): NormalizedQuestion | NormalizedQuestion[] {
  switch (offerRequestQuestion.type) {
    case "textarea":
      return normalizeTextAreaQuestion(offerRequestQuestion)
    case "date":
      return normalizeDateQuestion(offerRequestQuestion)
    case "number":
      return normalizeNumberQuestion(offerRequestQuestion)
    case "number_input":
      return normalizeNumberInputQuestion(offerRequestQuestion)
    case "list":
      return normalizeListPickerQuestion(offerRequestQuestion)
    case "radio":
      return normalizeRadioQuestion(offerRequestQuestion)
    case "checkbox":
      return normalizeCheckBoxQuestion(offerRequestQuestion)
    default:
      return null
  }
}

export function normalizeOfferRequestQuestions(
  offerRequest: OfferRequestDB,
): { [id: string]: NormalizedQuestion } {
  const normalizedData = {}
  offerRequest.questions.forEach(question => {
    let normalizedQuestion = normalizeOfferRequestQuestion(question)
    if (!normalizedQuestion) {
      return
    }
    normalizedQuestion = !Array.isArray(normalizedQuestion)
      ? [normalizedQuestion]
      : normalizedQuestion
    normalizedQuestion.forEach(data => {
      normalizedData[data.id] = data
    })
  })
  return normalizedData
}

const getAddressString = (address: Address): string =>
  `${address.city} ${address.postalCode} ${address.streetAddress}`

export function getNormalizedAddress(
  offerRequest: OfferRequestDB,
): string | { from: string; to: string } {
  const { addresses } = offerRequest
  if (addresses.type === "basic") {
    return getAddressString(addresses.main)
  }

  if (addresses.type === "delivery") {
    const from = getAddressString(addresses.from)
    const to = getAddressString(addresses.to)
    return { from, to }
  }
  return ""
}
