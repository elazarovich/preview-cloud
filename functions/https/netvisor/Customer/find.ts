import _ from "lodash"
import crypto from "crypto"
import axios from "axios"
import { parseString } from "xml2js"
import promisify from "util.promisify"
import {
  CUSTOMER_LIST_API_URL,
  LANG,
  SENDER,
  ORG_ID,
  CUSTOMER_ID,
  PARTNER_ID,
  ALGO,
  CUSTOMER_KEY,
  PARTNER_KEY,
} from "../config"

const parseStringPromisified = promisify(parseString)

export default async (name?: string) => {
  const URL = `${CUSTOMER_LIST_API_URL}?keyword=${name}`
  const TIMESTAMP = new Date().toISOString()
  const TRANS_ID = `ID${Date.now()}`
  // tslint:disable-next-line:max-line-length
  const hashSource = `${URL}&${SENDER}&${CUSTOMER_ID}&${TIMESTAMP}&${LANG}&${ORG_ID}&${TRANS_ID}&${CUSTOMER_KEY}&${PARTNER_KEY}`
  const hash = crypto
    .createHash("sha256")
    .update(hashSource)
    .digest("hex")

  const headers = {
    "X-Netvisor-Authentication-Sender": SENDER,
    "X-Netvisor-Authentication-CustomerId": CUSTOMER_ID,
    "X-Netvisor-Authentication-PartnerId": PARTNER_ID,
    "X-Netvisor-Interface-Language": LANG,
    "X-Netvisor-Organisation-ID": ORG_ID,
    "X-Netvisor-Authentication-MACHashCalculationAlgorithm": ALGO,
    "X-Netvisor-Authentication-Timestamp": TIMESTAMP,
    "X-Netvisor-Authentication-TransactionId": TRANS_ID,
  }

  headers["X-Netvisor-Authentication-MAC"] = hash

  try {
    const response = await axios({
      method: "get",
      responseType: "document",
      url: URL,
      data: {},
      headers,
    })
    const responseData = await parseStringPromisified(response.data)
    const customerList = _.get(responseData, "Root.Customerlist")
    if (Array.isArray(customerList) && !_.isEmpty(customerList[0])) {
      console.log(
        `Customer.find: found ${_.get(customerList[0], "Customer[0].Code[0]")}`,
      )
      return _.get(customerList[0], "Customer[0].Code[0]")
    }
    return null
  } catch (error) {
    console.error(`Customer.find: ${error}`)
  }
}
