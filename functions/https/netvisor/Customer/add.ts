import _ from "lodash"
import crypto from "crypto"
import axios from "axios"
import { parseString } from "xml2js"
import promisify from "util.promisify"
import {
  ADD_CUSTOMER_API_URL,
  LANG,
  SENDER,
  ORG_ID,
  CUSTOMER_ID,
  PARTNER_ID,
  ALGO,
  CUSTOMER_KEY,
  PARTNER_KEY,
} from "../config"

const parseStringPromisified = promisify(parseString)

const getHeaders = (URL: string) => {
  const TIMESTAMP = new Date().toISOString()
  const TRANS_ID = `ID${Date.now()}`
  // tslint:disable-next-line:max-line-length
  const hashSource = `${URL}&${SENDER}&${CUSTOMER_ID}&${TIMESTAMP}&${LANG}&${ORG_ID}&${TRANS_ID}&${CUSTOMER_KEY}&${PARTNER_KEY}`
  const hash = crypto
    .createHash("sha256")
    .update(hashSource)
    .digest("hex")
  return {
    "X-Netvisor-Authentication-Sender": SENDER,
    "X-Netvisor-Authentication-CustomerId": CUSTOMER_ID,
    "X-Netvisor-Authentication-PartnerId": PARTNER_ID,
    "X-Netvisor-Interface-Language": LANG,
    "X-Netvisor-Organisation-ID": ORG_ID,
    "X-Netvisor-Authentication-MACHashCalculationAlgorithm": ALGO,
    "X-Netvisor-Authentication-Timestamp": TIMESTAMP,
    "X-Netvisor-Authentication-TransactionId": TRANS_ID,
    "X-Netvisor-Authentication-MAC": hash,
    "Content-Type": "application/xml",
  }
}

const getCustomerXML = (props: {
  id: string
  name: string
  phone: string
  email: string
}) =>
  `<root>
        <customer>
            <customerbaseinformation>
                <internalidentifier>${props.id}</internalidentifier>
                <name>${props.name}</name>
                <phonenumber>${props.phone}</phonenumber>
                <email>${props.email}</email>
                <EmailInvoicingAddress>${props.email}</EmailInvoicingAddress>
            </customerbaseinformation>
        </customer>
    </root>`

export default async (props: {
  id: string
  name: string
  phone: string
  email: string
}) => {
  const URL = `${ADD_CUSTOMER_API_URL}`
  try {
    const customerXML = getCustomerXML(props)
    const response = await axios({
      method: "post",
      responseType: "document",
      url: URL,
      data: customerXML,
      headers: getHeaders(URL),
    })
    const responseData = await parseStringPromisified(response.data)
    const status = _.get(responseData, "Root.ResponseStatus[0].Status[0]")
    if (status === "OK") {
      console.log("Customer.add: customer added")
      return props.id
    }
    const message = _.get(responseData, "Root.ResponseStatus[0].Status[1]")
    console.error(`Customer.add: ${status} - ${message}`)
    console.log({ customerXML })
    return null
  } catch (error) {
    console.error(`Customer.add: ${error}`)
    return null
  }
}
