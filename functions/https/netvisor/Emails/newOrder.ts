import template from "./Template"
import { sendSupportEmail } from "../../../emails"

// tslint:disable-next-line:no-any
export default async (props: { orderData: any; customerData: any }) => {
  const { phone, name, email } = props.customerData
  const {
    id,
    category,
    description,
    date,
    time,
    address,
    addressTo,
    addressFrom,
  } = props.orderData
  const subject = `Tilauksesi #${id} on vastaanotettu`
  return sendSupportEmail({
    to: email,
    subject,
    html: template({
      phone,
      name,
      email,
      address,
      addressFrom,
      addressTo,
      description,
      category,
      date,
      time,
    }),
  })
}
