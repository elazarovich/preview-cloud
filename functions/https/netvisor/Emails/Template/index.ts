import header from "./header"
import footer from "./footer"
import moment = require("moment-timezone")

/* tslint:disable */

const customerInfo = (props: {
  name: string
  phone: string
  email: string
}) => `
    <tr>
        <td class="label">Nimi:</td>
        <td class="value strong">${props.name}</td>
    </tr>
    <tr>
        <td class="label">Puhelin:</td>
        <td class="value strong">${props.phone}</td>
    </tr>
    <tr>
        <td class="label">Email:</td>
        <td class="value strong">${props.email}</td>
    </tr>
`

const addresses = (props: {
  address: string
  addressFrom: string
  addressTo?: string
}) => {
  if (props.addressFrom && props.addressTo) {
    return `
            <tr>
                <td class="label">Lähtöosoite:</td>
                <td class="value strong">${props.addressFrom}</td>
            </tr>
            <tr>
                <td class="label">Määränpää:</td>
                <td class="value strong">${props.addressTo}</td>
            </tr>
        `
  }
  return `
        <tr>
            <td class="label">Osoite:</td>
            <td class="value strong">${props.addressFrom}</td>
        </tr>
    `
}

const orderInfo = (props: {
  description: string
  category: string
  date: Date
  time: string
}) => `
    <tr>
        <td class="label">Palvelu:</td>
        <td class="value strong">${props.category}</td>
    </tr>
    <tr>
        <td class="label">Pvm:</td>
        <td class="value strong">${moment(props.date).format("D.M.YYYY")}</td>
    </tr>
    <tr>
        <td class="label">Kellonaika:</td>
        <td class="value strong">${props.time}</td>
    </tr>`

export default (props: {
  phone: string
  name: string
  email: string
  address: string
  addressFrom: string
  addressTo?: string
  description: string
  category: string
  date: Date
  time: string
}) => {
  // tslint:disable:max-line-length
  const title = "Tilauksesi on vastaanotettu"
  const message =
    "Tarkistathan alta tilaustasi koskevat tiedot. Mikäli huomaat korjattavaa, voit vastata tähän viestiin."
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    ${header(title)}
    <body bgcolor="#f7f7f7">
        <table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
            <tr>
                <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;">
                    <center>
                        <table cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td class="header-lg">
                                    ${title}
                                </td>
                            </tr>
                            <tr>
                                <td class="free-text">
                                    <br/>
                                        ${message}
                                    <br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center>
                                    <table class="info-table">
                                        ${customerInfo(props)}
                                        ${orderInfo(props)}
                                        ${addresses(props)}
                                        <!--
                                        <tr>
                                            <td class="label">Kuvaus tekijälle:</td>
                                            <td class="value strong">"${
                                              props.description
                                            }"</td>
                                        </tr>
                                        -->
                                    </table>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 15px;">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                                        <tr>
                                            <td class="mini-block" style="font-size: 19px;">
                                                Seuraavaksi lähetämme arviolaskun jonka maksaminen vahvistaa tilauksen.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </center>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;">
                    <center>
                    <table cellspacing="0" cellpadding="0" width="600" style="border-collapse:separate !important;">
                        <tr>
                            <td style="padding: 0 30px; text-align:left;font-size: 16px;">
                                <span>Tilaamalla palvelun hyväksyt <a href="https://helpdor.com/kayttoehdot">käyttöehtomme</a></span><br/>
                                <span class="header-sm" style="line-height: 30px">Miksi Helpdor?</span>
                                Toimimme alustana joka auttaa asiakkaita löytämään oikeat tekijät helposti ja luotettavasti.
                                Laaja verkostomme takaa, että tarvitsitpa mitä palvelua tahansa, niin tekijä löytyy nopeasti.
                                <span class="header-sm" style="line-height: 30px">Miten Helpdor toimii?</span>
                                <table class="info-table">
                                    <tr>
                                        <td class="label">1.</td>
                                        <td class="value">Asiakas tekee tarjouspyynnön, jolloin teemme asiakkaalle arvion ja siihen perustuvan tarjouksen</td>
                                    </tr>
                                    <tr>
                                        <td class="label">2.</td>
                                        <td class="value">Asiakas saa arviolaskun jonka maksaminen vahvistaa sovitun työn <br/><span style="font-size: 14px; color: gray">Joissain palveluissa, kuten remonteissa, saatetaan sopia toisenlaisesta laskutuksesta.</span></td>
                                    </tr>
                                    <tr>
                                        <td class="label">3.</td>
                                        <td class="value">Kysymme asiakkaalta palautteen ja vahvistuksen työn kestosta</td>
                                    </tr>
                                    <tr>
                                        <td class="label">4.</td>
                                        <td class="value">Mikäli työ tehtiin alkuperäistä arviota nopeammin palautamme erotuksen asiakkaalle</td>
                                    </tr>
                                </table>
                                <span class="header-sm" style="line-height: 30px">Mistä Helpdor vastaa?</span>
                                Helpdor huolehtii siitä että kaikki välittämämme tekijät ovat luotettavia ja että palvelun toteuttava yritys on hankkinut tarvittavat vakuutukset palvelun toimittamiseen.

                                Reklaamaatiot ovat harvinaisia, mutta mikäli sellaiseen tulee aihetta, autamme asiakasta ja toimimme välittäjänä asiakkaan sekä palvelun toimittaneen ja siitä vastaavan tekijän välillä.
                                <br/><a style="width:100%; display:block;" href="https://www.tilaajavastuu.fi/?yt=2844881-9" target="_blank">
                                    <img style="width:40%; margin: 10px auto; display:block;" src="https://helpdor.com/wp-content/uploads/2018/11/luotettava-kumppani_300x187_polygon-1.png" alt="">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 15px;">
                                <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                                    <tr>
                                        <td class="mini-block" style="font-size: 19px; text-align:center;">
                                            <span class="header-sm" style="line-height: 30px; padding: 0 !important">Tarvitsetko muita palveluita?</span>
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/siivous/">Siivouspalvelut</a> -
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/muutto/">Muuttopalvelu</a> -
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/pihatyot/">Pihatyöt</a> -
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/handyman/">Handyman</a><br/> -
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/remontit/">Erilaiset remontit</a> -
                                            <a target="_blank" style="font-size: 16px;" href="https://helpdor.com/loyda-osaaja/">Putki- ja sähkötyöt</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </center>
                </td>
            </tr>
            ${footer()}
        </table>
    </div>
</body>
</html>`
}
// tslint:enable:max-line-length
