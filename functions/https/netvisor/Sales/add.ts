import _ from "lodash"
import crypto from "crypto"
import axios from "axios"
import { parseString } from "xml2js"
import promisify from "util.promisify"
import {
  CUSTOMER_SALE_API_URL,
  LANG,
  SENDER,
  ORG_ID,
  CUSTOMER_ID,
  PARTNER_ID,
  ALGO,
  CUSTOMER_KEY,
  PARTNER_KEY,
} from "../config"

const parseStringPromisified = promisify(parseString)

const getHeaders = (URL: string) => {
  const TIMESTAMP = new Date().toISOString()
  const TRANS_ID = `ID${Date.now()}`
  // tslint:disable-next-line:max-line-length
  const hashSource = `${URL}&${SENDER}&${CUSTOMER_ID}&${TIMESTAMP}&${LANG}&${ORG_ID}&${TRANS_ID}&${CUSTOMER_KEY}&${PARTNER_KEY}`
  const hash = crypto
    .createHash("sha256")
    .update(hashSource)
    .digest("hex")
  return {
    "X-Netvisor-Authentication-Sender": SENDER,
    "X-Netvisor-Authentication-CustomerId": CUSTOMER_ID,
    "X-Netvisor-Authentication-PartnerId": PARTNER_ID,
    "X-Netvisor-Interface-Language": LANG,
    "X-Netvisor-Organisation-ID": ORG_ID,
    "X-Netvisor-Authentication-MACHashCalculationAlgorithm": ALGO,
    "X-Netvisor-Authentication-Timestamp": TIMESTAMP,
    "X-Netvisor-Authentication-TransactionId": TRANS_ID,
    "X-Netvisor-Authentication-MAC": hash,
    "Content-Type": "application/xml",
  }
}

const getCategoryName = (id: string, categoryName: string) => {
  if (categoryName) {
    return categoryName
  }

  switch (id) {
    case "MUUTTO":
      return "Muuttopalvelu"
    case "KANTOAPU":
      return "Kantoapu"
    case "HANDYMAN":
      return "Handymanpalvelu"
    case "PIHATYÖ":
      return "Pihatyöpalvelu"
    case "PUTKIMIES":
      return "Putkimiespalvelu"
    case "SÄHKÖMIES":
      return "Sähkömiespalvelu"
    case "REMONTTI":
      return "Remonttipalvelu"
    case "SIIVOUS":
      return "Siivouspalvelu"
    case "IKKUNANPESU":
      return "Ikkunoiden pesu"
    case "IV":
      return "IV-työ"
    case "LVI":
      return "LVI-työ"
    default:
      return "Palvelu"
  }
}

const getFreeText = (props: {
  category: string
  categoryName: string
  address: string
  addressFrom: string
  addressTo?: string
  date: string
  time: string
}) => {
  let baseText = `TILAUSVAHVISTUS/VARAUSMAKSU: ${getCategoryName(
    props.category,
    props.categoryName,
  )}, `
  const addressMain = [props.address, props.addressFrom, " "].find(
    a => a && a.length,
  )
  if (props.addressTo) {
    baseText = `${baseText} ${addressMain} - ${props.addressTo}, `
  } else {
    baseText = `${baseText} ${addressMain}, `
  }
  const deliveryDateObject = new Date(props.date)
  // tslint:disable-next-line:max-line-length
  const deliveryDateString = `${deliveryDateObject.getFullYear()}-${deliveryDateObject.getMonth() +
    1}-${deliveryDateObject.getDate()}`
  return `${baseText} ${deliveryDateString} ${props.time}`
}

const getSalesXML = (props: {
  id: string
  pricePerHour: number
  duration: number
  category: string
  categoryName: string
  description: string
  date: string
  time: string
  address: string
  addressTo?: string
  addressFrom: string
  customerId: string
  customerName: string
  customerEmail: string
  customerPhone: string
}) => {
  const dateObject = new Date()
  const invoiceDateString = `${dateObject.getFullYear()}-${dateObject.getMonth() +
    1}-${dateObject.getDate()}`

  const deliveryAddress = [
    props.address,
    props.addressFrom,
    props.addressTo,
    " ",
  ].find(a => a && a.length)
  // tslint:disable:max-line-length
  return `<root>
            <salesinvoice>
                <salesinvoicenumber>${props.id}000</salesinvoicenumber>
                <salesinvoicedate format="ansi">${invoiceDateString}</salesinvoicedate>
                <salesinvoiceduedate format="ansi">${invoiceDateString}</salesinvoiceduedate>
                <salesinvoicedeliverydate format="ansi">${
                  props.date
                }</salesinvoicedeliverydate>
                <salesinvoiceamount>${props.pricePerHour *
                  props.duration}</salesinvoiceamount>
                <sellername>Helpdor Oy</sellername>
                <invoicetype>order</invoicetype>
                <salesinvoicestatus type="netvisor">undelivered</salesinvoicestatus>
                <salesinvoicefreetextbeforelines>${getFreeText(
                  props,
                )}</salesinvoicefreetextbeforelines>
                <salesinvoiceprivatecomment>${
                  props.description
                }</salesinvoiceprivatecomment>
                <Invoicingcustomeridentifier type="customer">${
                  props.customerId
                }</Invoicingcustomeridentifier>
                <invoicingcustomername>${
                  props.customerName
                }</invoicingcustomername>
                <invoicingcustomeraddressline>Email: ${props.customerEmail ||
                  "-"}</invoicingcustomeraddressline>
                <invoicingcustomeradditionaladdressline>Puhelin: ${props.customerPhone ||
                  "-"}</invoicingcustomeradditionaladdressline>
                <deliveryaddressline>${deliveryAddress &&
                  deliveryAddress.substring(0, 99)}</deliveryaddressline>
                <deliveryterm>Sopimuksen mukaan</deliveryterm>
                <invoicelines>
                    <invoiceline>
                        <salesinvoiceproductline>
                            <productidentifier type="customer">${
                              props.category
                            }</productidentifier>
                            <productname>${getCategoryName(
                              props.category,
                              props.categoryName,
                            )}</productname>
                            <productunitprice type="gross">${
                              props.pricePerHour
                            }</productunitprice>
                            <productvatpercentage vatcode="KOMY">24</productvatpercentage>
                            <salesinvoiceproductlinequantity>${
                              props.duration
                            }</salesinvoiceproductlinequantity>
                        </salesinvoiceproductline>
                    </invoiceline>
                </invoicelines>
            </salesinvoice>
        </root>`
  // tslint:enable:max-line-length
}

export default async (props: {
  id: string
  pricePerHour: number
  duration: number
  category: string
  categoryName: string
  description: string
  date: string
  time: string
  address: string
  addressTo?: string
  addressFrom: string
  customerId: string
  customerName: string
  customerEmail: string
  customerPhone: string
}) => {
  const URL = `${CUSTOMER_SALE_API_URL}`

  try {
    const salesXML = getSalesXML(props)
    const response = await axios({
      method: "post",
      responseType: "document",
      url: URL,
      data: salesXML,
      headers: getHeaders(URL),
    })
    const responseData = await parseStringPromisified(response.data)

    const responseStatus = _.get(
      responseData,
      "Root.ResponseStatus[0].Status[0]",
    )
    if (responseStatus === "FAILED") {
      const responseMessage = _.get(
        responseData,
        "Root.ResponseStatus[0].Status[1]",
      )
      const errorMessage = `${responseStatus}: ${responseMessage}`
      console.error(`Sales.add: ${errorMessage}`)
      console.log({ salesXML })
      return { status: errorMessage, success: false }
    }
    console.log(`Sales.add: ${responseStatus}`)
    return { status: responseStatus, success: true }
  } catch (error) {
    console.error(`Sales.add: ${error}`)
    return { status: error, success: false }
  }
}
