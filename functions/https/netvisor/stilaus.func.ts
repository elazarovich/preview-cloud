import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import isEmail from "validator/lib/isEmail"
import findCustomer from "./Customer/find"
import addCustomer from "./Customer/add"
import addSales from "./Sales/add"
import sendNewOrderEmail from "./Emails/newOrder"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const encodeToXml = (s?: string) => {
  if (s && s.length > 0) {
    return s
      .replace(/&/g, "ja")
      .replace(/"/g, " ")
      .replace(/'/g, " ")
      .replace(/</g, "")
      .replace(/>/g, "")
      .replace(/\t/g, "")
      .replace(/\n/g, "")
      .replace(/\r/g, "")
  } else {
    return s
  }
}

const modifyTextToXML = (text?: string) => {
  if (text && text.length > 0) {
    const encodedText = encodeToXml(text)
    return encodedText && encodedText.substring(0, 254)
  } else {
    return text
  }
}

// tslint:disable-next-line:no-any
const getCustomerData = (data: any) => {
  const customerData = {
    id: undefined as any, // tslint:disable-line:no-any
    ytunnus: data.Ytunnus, // tslint:disable-line:no-unsafe-any
    phone: data["Phone number"], // tslint:disable-line:no-unsafe-any
    name: modifyTextToXML(data.Name as string | undefined), // tslint:disable-line:no-unsafe-any
    email: data.Email, // tslint:disable-line:no-unsafe-any
  }

  if (customerData.ytunnus) {
    customerData.id = customerData.ytunnus.trim() // tslint:disable-line:no-unsafe-any
  } else if (customerData.phone && customerData.phone.length) {
    // tslint:disable-line:no-unsafe-any
    let id: string = customerData.phone // tslint:disable-line:no-unsafe-any
    id = id.trim()
    id = id.replace(/^\++/, "")
    id = id.replace(/^0+/, "")
    id = id.split(" ").join("")
    id = id.split("-").join("")
    customerData.id = id
  } else if (
    customerData.email &&
    customerData.email.length && // tslint:disable-line:no-unsafe-any
    isEmail(customerData.email) // tslint:disable-line:no-unsafe-any
  ) {
    customerData.id = customerData.email.trim() // tslint:disable-line:no-unsafe-any
  } else {
    customerData.id = data["Merkinnän ID"] // tslint:disable-line:no-unsafe-any
  }
  return customerData
}

// tslint:disable-next-line:no-any
const getOrderData = (data: any) => ({
  id: data["Merkinnän ID"], // tslint:disable-line:no-unsafe-any
  pricePerHour: data["Price/h"], // tslint:disable-line:no-unsafe-any
  duration: data.Duration, // tslint:disable-line:no-unsafe-any
  category: data["Service category"], // tslint:disable-line:no-unsafe-any
  categoryName: data.Palvelu, // tslint:disable-line:no-unsafe-any
  description: modifyTextToXML(data.Details as string | undefined), // tslint:disable-line:no-unsafe-any
  date: data.Date, // tslint:disable-line:no-unsafe-any
  time: data.Time, // tslint:disable-line:no-unsafe-any
  address: modifyTextToXML(data.Address as string | undefined), // tslint:disable-line:no-unsafe-any
  addressTo: modifyTextToXML(data["Address to"]), // tslint:disable-line:no-unsafe-any
  addressFrom: modifyTextToXML(data["Address from"]), // tslint:disable-line:no-unsafe-any
})

export const newOrder = functions.https.onRequest(async (req, res) => {
  const data = req.body
  const customerData = getCustomerData(data)
  const orderData = getOrderData(data)
  console.log(`STILAUS: find customer: ${customerData.id}`)
  const customerIdFound = await findCustomer(customerData.id)
  if (!customerIdFound) {
    console.log(`STILAUS: not found: add ${customerData.id}`)
    await addCustomer(customerData as any) // tslint:disable-line:no-any
  } else {
    customerData.id = customerIdFound // tslint:disable-line:no-unsafe-any
    console.log(`STILAUS: add sales to ${customerData.id}`)
  }

  const { status, success } = await addSales({
    customerId: customerData.id,
    customerName: customerData.name,
    customerEmail: customerData.email,
    customerPhone: customerData.phone,
    ...orderData,
  } as any) // tslint:disable-line:no-any

  // tslint:disable-next-line:no-unsafe-any
  if (success && data.Tilausvahvistus === "Kyllä") {
    await sendNewOrderEmail({ orderData, customerData })
  }
  if (success) {
    res.status(200).send(status)
  } else {
    res.status(500).send(status)
  }
})

export const testNewOrderEmail = functions.https.onRequest(async (req, res) => {
  await sendNewOrderEmail({
    orderData: {
      id: Math.floor(Math.random() * 10000),
      category: "MUUTTO",
      description: "afdaflk dsjaflkdsjaf ldsaj fdls.",
      date: "2018-10-02",
      time: "19:00",
      addressTo: "Sepontie 17 G 19 00630 Helsinki",
      addressFrom: "Sepposenntie 14 F 007 00780 Helsinki",
    },
    customerData: {
      phone: "0452151560",
      name: "Seppo Sepponen",
      email: "janne@helpdor.com",
    },
  })
  res.status(200).send("sent")
})

export const testAddCustomer = functions.https.onRequest(async (req, res) => {
  const added = await addCustomer({
    id: Date.now().toString(),
    name: "Testi Käyttäjä",
    phone: "23454543435",
    email: `janne.vickholm+${Date.now()}@helpdor.com`,
  })
  if (added) {
    res.status(200).send("sent")
  } else {
    res.status(500).send(added)
  }
})
