import * as functions from "firebase-functions"

// tslint:disable-next-line:no-unsafe-any
const partnerKey: string = functions.config().netvisor.partner_key
// tslint:disable-next-line:no-unsafe-any
const customerKey: string = functions.config().netvisor.customer_key
// tslint:disable-next-line:no-unsafe-any
const partnerId: string = functions.config().netvisor.partner_id
// tslint:disable-next-line:no-unsafe-any
const customerId: string = functions.config().netvisor.customer_id

export const ADD_CUSTOMER_API_URL =
  "https://integration.netvisor.fi/customer.nv?Method=add"
export const CUSTOMER_LIST_API_URL =
  "https://integration.netvisor.fi/customerlist.nv"
export const CUSTOMER_SALE_API_URL =
  "https://integration.netvisor.fi/salesinvoice.nv?Method=add"
export const GET_INVOICE_LIST_URL =
  "https://integration.netvisor.fi/salesinvoicelist.nv"
export const GET_INVOICE_URL =
  "https://integration.netvisor.fi/getsalesinvoice.nv"
export const GET_CUSTOMER_URL = "https://integration.netvisor.fi/getcustomer.nv"
export const PARTNER_ID = partnerId
export const PARTNER_KEY = partnerKey
export const CUSTOMER_ID = customerId
export const CUSTOMER_KEY = customerKey
export const SENDER = "Test Requests"
export const LANG = "FI"
export const ORG_ID = "2844881-9"
export const ALGO = "SHA256"
