import Expo, { ExpoPushMessage } from "expo-server-sdk"
import * as admin from "firebase-admin"
import { REF_NAMES } from "./db/RefNames"
import { PushNotificationData } from "helpdor-types"
const { USERS, NOTIFICATIONS } = REF_NAMES

try {
    admin.initializeApp()
} catch (e) {
    // The admin SDK can only be initialized once.
    // If it has been done in another file we need to handle the error
}

const expo = new Expo()

export interface NotificationMessage extends ExpoPushMessage {
  userId: string,
  data: PushNotificationData
}

const saveNotification = async (message: NotificationMessage) => {
    const databaseRef = admin.app().database()
    const notificationsRef = await databaseRef.ref(`/${NOTIFICATIONS}`)
    const { key } = await notificationsRef.push({
        createdAt: admin.database.ServerValue.TIMESTAMP,
        message: message.data.message,
        meta: message.data.meta
    })
    if (!key) {
      return
    }
    await databaseRef.ref(`/${USERS}`)
        .child(message.userId)
        .child("notifications")
        .child(key)
        .set(true)
}

export const saveNotificationToDB = async (messages: NotificationMessage[]) => {
    if (messages.length) {
        return Promise.all(
            messages.map(message => saveNotification(message))
        )
    }
    return true
}

export const sendPushNotification = async (messages: ExpoPushMessage[]) => {
    if (messages.length) {
        const pushNotification = await expo.sendPushNotificationsAsync(messages)
        return pushNotification
    }
    return true
}
