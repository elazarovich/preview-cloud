import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../RefNames"
import { ReviewDB } from "helpdor-types"

const { REVIEWS, SELLER_PROFILES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

// When new Review is created, updates the SellerProfile.reviews with a reference to that Review
const addReviewToSellerProfile = (
  snap: functions.database.DataSnapshot,
  context: functions.EventContext,
) => {
  // tslint:disable-next-line:no-unsafe-any
  const review: ReviewDB = snap.val()
  return admin
    .app()
    .database()
    .ref(`/${SELLER_PROFILES}`)
    .child(review.sellerProfile)
    .child(REVIEWS)
    .child(context.params.pushId as string)
    .set(true)
}

export const addReviewToSeller = functions.database
  .ref(`/${REVIEWS}/{pushId}`)
  .onCreate((snap, context) => addReviewToSellerProfile(snap, context))
