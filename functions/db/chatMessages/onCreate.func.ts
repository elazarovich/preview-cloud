import * as functions from "firebase-functions"
import _ from "lodash"
import * as admin from "firebase-admin"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { REF_NAMES } from "../RefNames"
import { getUsers } from "../users"
import {
  OfferDB,
  ChatDB,
  BuyerProfileDB,
  SellerProfileDB,
  UserDB,
  ChatMessageDB,
  PushNotificationMeta,
} from "helpdor-types"

const {
  CHAT_MESSAGES,
  CHATS,
  OFFERS,
  BUYER_PROFILES,
  SELLER_PROFILES,
} = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const getSenderProfile = async (
  id: string,
  role: "seller" | "buyer",
  databaseInstance: admin.database.Database,
): Promise<BuyerProfileDB | SellerProfileDB | null> => {
  if (role === "seller") {
    const sellerProfileSnapshot = await databaseInstance
      .ref(`/${SELLER_PROFILES}`)
      .child(id)
      .once("value")
    return sellerProfileSnapshot.val() as SellerProfileDB
  } else if (role === "buyer") {
    const buyerProfileSnapshot = await databaseInstance
      .ref(`/${BUYER_PROFILES}`)
      .child(id)
      .once("value")
    return buyerProfileSnapshot.val() as BuyerProfileDB
  } else {
    return null
  }
}

const composeNotifications = (
  users: Array<UserDB & { id: string }>,
  title: string,
  body: string,
  meta: PushNotificationMeta,
) =>
  users
    .filter(user => user.pushToken)
    .filter(user => {
      if (
        user.activeRoute &&
        user.activeRoute.chatId &&
        (meta.type === "chatMessageForSeller" ||
          meta.type === "chatMessageForBuyer") &&
        meta.chatId === user.activeRoute.chatId
      ) {
        return false
      }
      return true
    })
    .map(user => ({
      userId: user.id,
      to: user.pushToken
        ? `ExponentPushToken[${Object.keys(user.pushToken)[0]}]`
        : "",
      title,
      body,
      data: {
        message: {
          title,
          body,
        },
        meta,
      },
    }))

const getAllReceivers = async (
  members: ChatDB["members"],
  senderId: string,
  databaseInstance: admin.database.Database,
) => {
  const receiverIds = Object.keys(members).filter(
    memberId => memberId !== senderId,
  )
  return getUsers(databaseInstance, receiverIds)
}

// When new Chat message is created, send notifications about it
const broadcastChatMessageNotification = async (
  snap: functions.database.DataSnapshot,
  context: functions.EventContext,
) => {
  const databaseInstance = admin.app().database()

  // tslint:disable-next-line:no-unsafe-any
  const { chat, content, sender }: ChatMessageDB = snap.val()

  const chatsSnapshot = await databaseInstance
    .ref(`/${CHATS}`)
    .child(chat)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const { members, offer: offerId }: ChatDB = chatsSnapshot.val()
  if (_.isEmpty(members)) {
    console.error(new Error("Chat has no members!"))
  } else if (!members[sender]) {
    console.error(
      new Error(`Sender (${sender}) of a message is not a member or the chat!`),
    )
  }

  const offerSnapshot = await databaseInstance
    .ref(`/${OFFERS}`)
    .child(offerId)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const { offerRequest: offerRequestId }: OfferDB = offerSnapshot.val()

  const senderProfile = await getSenderProfile(
    sender,
    members[sender].role,
    databaseInstance,
  )

  const receivers = await getAllReceivers(members, sender, databaseInstance)

  const title =
    members[sender].role === "seller"
      ? `${(senderProfile as SellerProfileDB).companyName}:`
      : `${(senderProfile as BuyerProfileDB).displayName}:`
  const body = content
  const meta: PushNotificationMeta = {
    type:
      members[sender].role === "seller"
        ? "chatMessageForBuyer"
        : "chatMessageForSeller",
    chatId: chat,
    offerRequestId,
    offerId,
    messageId: context.params.pushId as string,
  }

  const notifications = composeNotifications(receivers, title, body, meta)

  await saveNotificationToDB(notifications)
  await sendPushNotification(notifications)
}

export const sendNotification = functions.database
  .ref(`/${CHAT_MESSAGES}/{pushId}`)
  .onCreate((snap, context) => broadcastChatMessageNotification(snap, context))
