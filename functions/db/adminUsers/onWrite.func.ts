import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../RefNames"
import { UserDB } from "helpdor-types"

const { ADMIN_USERS, USERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const IS_ADMIN = "isAdmin"

export const setAdminStatus = functions.database
  .ref(`/${ADMIN_USERS}/{pushId}`)
  .onCreate(async (snap, context) => {
    const isAdmin = snap.val()
    const userId = context.params.pushId as string
    const userRef = admin
      .app()
      .database()
      .ref(`${USERS}/${userId}`)
    await userRef.child(IS_ADMIN).set(isAdmin)
  })

export const removeAdminStatus = functions.database
  .ref(`/${ADMIN_USERS}/{pushId}`)
  .onDelete(async (snap, context) => {
    const userId = context.params.pushId as string
    const userRef = admin
      .app()
      .database()
      .ref(`${USERS}/${userId}`)
    const userSnapshot = await userRef.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const user: UserDB = userSnapshot.val()
    if (user.isAdmin) {
      await userRef.child(IS_ADMIN).set(false)
    }
  })
