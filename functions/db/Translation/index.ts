import _ from "lodash"
import English from "./English.json"
import Finnish from "./Finnish.json"

export enum Locales {
  EN = "en",
  FI = "fi",
}

export function getAvailableLocale(locale: string): Locales {
  return [Locales.EN, Locales.FI].includes(locale as Locales)
    ? (locale as Locales)
    : Locales.EN
}

const getLocalizedDict = (locale: string) => {
  switch (locale) {
    case Locales.FI:
      return Finnish
    case Locales.EN:
      return English
    default:
      return English
  }
}

export const Translate = (locale: string, text: string) => {
  const dict = getLocalizedDict(locale)
  return _.get(dict, text, text)
}
