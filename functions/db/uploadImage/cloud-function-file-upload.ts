import * as functions from "firebase-functions"
import Busboy from "busboy"
import os from "os"
import path from "path"
import fs from "fs"

export interface UploadedFile {
  fieldName: string
  originalName: string
  encoding: string
  mimeType: string
  buffer: Buffer
  size: number
}

export function prepareFilesToUpload(
  request: functions.https.Request & { files?: UploadedFile[] },
  response: functions.Response,
  next: () => void,
) {
  const busboy = new Busboy({
    headers: request.headers,
    limits: {
      fileSize: 10 * 1024 * 1024,
    },
  })

  const fields = {}
  const files: UploadedFile[] = []
  const fileWrites: Array<Promise<void>> = []
  const tmpdir = os.tmpdir()

  busboy.on("field", (key, value) => {
    fields[key] = value
  })

  busboy.on("file", (fieldName, file, fileName, encoding, mimeType) => {
    const filepath = path.join(tmpdir, fileName)
    const writeStream = fs.createWriteStream(filepath)
    file.pipe(writeStream)

    fileWrites.push(
      new Promise((resolve, reject) => {
        file.on("end", () => writeStream.end())
        writeStream.on("finish", () => {
          fs.readFile(filepath, (err, buffer) => {
            const size = Buffer.byteLength(buffer)
            if (err) {
              return reject(err)
            }

            files.push({
              fieldName,
              originalName: fileName,
              encoding,
              mimeType,
              buffer,
              size,
            })

            try {
              fs.unlinkSync(filepath)
            } catch (error) {
              return reject(error)
            }

            resolve()
          })
        })
        writeStream.on("error", reject)
      }),
    )
  })

  busboy.on("finish", () => {
    Promise.all(fileWrites)
      .then(() => {
        request.body = fields
        request.files = files
        next()
      })
      .catch(next)
  })

  busboy.end(request.rawBody)
}
