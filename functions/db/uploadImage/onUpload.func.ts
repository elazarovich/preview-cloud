import * as admin from "firebase-admin"
import * as functions from "firebase-functions"
import Cors from "cors"
import express from "express"
import { MetadataResponse } from "@google-cloud/common"
import {
  prepareFilesToUpload,
  UploadedFile,
} from "./cloud-function-file-upload"

const app = express().use(Cors({ origin: true }))

try {
  admin.initializeApp()
} catch (e) {
  // console.log("Exception", e);
}

const uploadImageToStorage = (
  file: UploadedFile,
): Promise<MetadataResponse> => {
  const storage = admin.storage()
  return new Promise((resolve, reject) => {
    const fileUpload = storage.bucket().file(file.fieldName + file.originalName)

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: "image/jpg",
      },
    })

    blobStream.on("error", error => reject(error))

    blobStream.on("finish", () => {
      fileUpload
        .getMetadata()
        .then(metadata => {
          resolve(metadata)
        })
        .catch(error => reject(error))
    })

    blobStream.end(file.buffer)
  })
}

app.post(
  "/picture",
  prepareFilesToUpload,
  (
    req: functions.https.Request & { files: UploadedFile[] },
    resp: functions.Response,
    next: () => void,
  ) => {
    uploadImageToStorage(req.files[0])
      .then(metadata => {
        resp.status(200).json(metadata[0])
        next()
      })
      .catch(error => {
        resp.status(500).json({ error })
        next()
      })
  },
)

export const api = functions.https.onRequest(app)
