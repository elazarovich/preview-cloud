import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../RefNames"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { Translate } from "../Translation"
import { UserDB, PushNotificationMeta } from "helpdor-types"

const { APPROVED_SELLERS, USERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}
const notifyUser = async (
  userRef: admin.database.Reference,
  userId: string,
) => {
  const userSnapshot = await userRef.once("value")
  // tslint:disable-next-line:no-unsafe-any
  const user: UserDB = userSnapshot.val()
  if (!user.pushToken) {
    console.log(`do not notify ${userId} who has no pushToken!`)
    return
  }
  const userLanguage = user.language
  const title = Translate(userLanguage, "approved")
  const body = Translate(userLanguage, "approved_for_selling")
  const meta: PushNotificationMeta = {
    type: "approvedSeller",
  }
  const notifications = [
    {
      userId,
      to: `ExponentPushToken[${Object.keys(user.pushToken)[0]}]`,
      sound: "default" as "default",
      title,
      body,
      data: {
        message: {
          title,
          body,
        },
        meta,
      },
    },
  ]
  await saveNotificationToDB(notifications)
  await sendPushNotification(notifications)
}

const IS_APPROVED_SELLER = "isApprovedSeller"
const removeApprovalStatus = async (userId: string) => {
  const userRef = admin
    .app()
    .database()
    .ref(`${USERS}/${userId}`)
  const userSnapshot = await userRef.once("value")
  // tslint:disable-next-line:no-unsafe-any
  const user: UserDB = userSnapshot.val()
  if (user.isApprovedSeller) {
    await userRef.child("isApprovedSeller").set(false)
  }
}
const updateUserAndNotify = async (
  isApproved: boolean,
  context: functions.EventContext,
) => {
  const userId = context.params.pushId as string
  const userRef = admin
    .app()
    .database()
    .ref(`${USERS}/${userId}`)
  await userRef.child(IS_APPROVED_SELLER).set(isApproved)
  if (isApproved) {
    await notifyUser(userRef, userId)
  }
}

export const createApproveStatus = functions.database
  .ref(`/${APPROVED_SELLERS}/{pushId}`)
  .onCreate((snap, context) =>
    updateUserAndNotify(snap.val() as boolean, context),
  )

export const updateApproveStatus = functions.database
  .ref(`/${APPROVED_SELLERS}/{pushId}`)
  .onUpdate((change, context) =>
    updateUserAndNotify(change.after.val() as boolean, context),
  )

export const removeApproveStatus = functions.database
  .ref(`/${APPROVED_SELLERS}/{pushId}`)
  .onDelete((snap, context) =>
    removeApprovalStatus(context.params.pushId as string),
  )
