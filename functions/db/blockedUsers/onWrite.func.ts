import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../RefNames"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { Translate } from "../Translation"
import { PushNotificationMeta, UserDB } from "helpdor-types"

const { BLOCKED_USERS, USERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}
const notifyUser = async (
  userRef: admin.database.Reference,
  userId: string,
) => {
  const userSnapshot = await userRef.once("value")
  // tslint:disable-next-line:no-unsafe-any
  const user: UserDB = userSnapshot.val()
  const userLanguage = user.language
  const title = Translate(userLanguage, "approved")
  const body = Translate(userLanguage, "unblocked_user")
  const meta: PushNotificationMeta = {
    type: "unblockedUser",
  }
  const notifications = [
    {
      userId,
      to: user.pushToken
        ? `ExponentPushToken[${Object.keys(user.pushToken)[0]}]`
        : "",
      title,
      body,
      data: {
        message: {
          title,
          body,
        },
        meta,
      },
    },
  ]
  await saveNotificationToDB(notifications)
  await sendPushNotification(notifications)
}

const updateUserAndNotify = async (
  isBlocked: boolean,
  context: functions.EventContext,
) => {
  const userId = context.params.pushId as string
  const userRef = admin
    .app()
    .database()
    .ref(`${USERS}/${userId}`)
  await userRef.child("isBlocked").set(isBlocked)
  if (!isBlocked) {
    await notifyUser(userRef, userId)
  }
}

export const createBlockedStatus = functions.database
  .ref(`/${BLOCKED_USERS}/{pushId}`)
  .onCreate((snap, context) =>
    updateUserAndNotify(snap.val() as boolean, context),
  )

export const updateBlockedStatus = functions.database
  .ref(`/${BLOCKED_USERS}/{pushId}`)
  .onUpdate((change, context) =>
    updateUserAndNotify(change.after.val() as boolean, context),
  )

export const removeBlockedStatus = functions.database
  .ref(`/${BLOCKED_USERS}/{pushId}`)
  .onDelete((snap, context) => {
    return admin
      .app()
      .database()
      .ref(`${USERS}/${context.params.pushId}`)
      .child("isBlocked")
      .set(false)
  })
