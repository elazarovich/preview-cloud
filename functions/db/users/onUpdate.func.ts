import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { sendVerificationCode } from "../../sms"
import { getUser } from "./index"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const verifyUserPhone = async (
  snapshot: functions.database.DataSnapshot,
  userId: string,
) => {
  const user = await getUser(admin.app().database(), userId)
  if (user.isCreatedFromWeb) {
    return
  }

  // tslint:disable-next-line:no-unsafe-any
  const phoneAfter: string = snapshot.val()

  console.log(`User phone changed to ${phoneAfter}`)

  if (phoneAfter && snapshot.ref.parent) {
    await snapshot.ref.parent.child("phoneVerified").set(false)

    const { code, msg } = await sendVerificationCode(phoneAfter)

    void admin
      .app()
      .database()
      .ref(REF_NAMES.CONFIRMATION_CODES)
      .child(userId)
      .set({ code, createdAt: admin.database.ServerValue.TIMESTAMP })

    console.log(`Confirmation code sent (${msg} to ${phoneAfter})`)
  } else {
    console.log("Verify phone called, but phone is unchanged or empty")
  }
}

export const updatePhone = functions.database
  .ref(`/${REF_NAMES.USERS}/{userId}/phone`)
  .onUpdate((change, context) =>
    verifyUserPhone(change.after, context.params.userId as string),
  )

export const createPhone = functions.database
  .ref(`/${REF_NAMES.USERS}/{userId}/phone`)
  .onCreate((snap, context) =>
    verifyUserPhone(snap, context.params.userId as string),
  )
