import { REF_NAMES } from "../RefNames"
import admin from "firebase-admin"
import { UserDB, SellerProfileDB } from "helpdor-types"

const { USERS, SELLER_PROFILES } = REF_NAMES

export interface User extends UserDB {
  id: string
}

export interface SellerProfile extends SellerProfileDB {
  id: string
}

export const getUser: (
  database: admin.database.Database,
  userId: string,
) => Promise<User> = async (database, userId) => {
  const userSnapshot = await database
    .ref(`/${USERS}`)
    .child(userId)
    .once("value")
  const user: UserDB = userSnapshot.val() // tslint:disable-line:no-unsafe-any
  return {
    id: userId,
    ...user,
  }
}

export const getUsers: (
  database: admin.database.Database,
  userIds: string[],
) => Promise<User[]> = async (database, userIds) => {
  return await Promise.all(userIds.map(id => getUser(database, id)))
}

export const getAllSellers = async (database: admin.database.Database) => {
  // tslint:disable-next-line:no-unsafe-any
  const sellers: admin.database.DataSnapshot = await database
    .ref(`/${SELLER_PROFILES}`)
    .once("value")
  const sellerIds: string[] = Object.keys(sellers.val()) // tslint:disable-line:no-unsafe-any
  return getUsers(database, sellerIds)
}

export const getAllSellersForCategory: (
  database: admin.database.Database,
  category: string,
) => Promise<User[]> = async (database, category) => {
  const snapshot = await database.ref(`/${SELLER_PROFILES}`).once("value")

  const sellerIds: string[] = []
  snapshot.forEach(sellerSnapShot => {
    const seller: SellerProfileDB = sellerSnapShot.val() // tslint:disable-line:no-unsafe-any
    if (
      sellerSnapShot.key &&
      seller.categories &&
      Object.keys(seller.categories).includes(category)
    ) {
      sellerIds.push(sellerSnapShot.key)
    }
  })

  return getUsers(database, sellerIds)
}

export const getSellerProfile: (
  database: admin.database.Database,
  userId: string,
) => Promise<SellerProfile | undefined> = async (database, userId) => {
  const userSnapshot = await database
    .ref(`/${SELLER_PROFILES}`)
    .child(userId)
    .once("value")
  const user: SellerProfileDB | null = userSnapshot.val() // tslint:disable-line:no-unsafe-any
  if (user && userSnapshot.key) {
    return {
      id: userId,
      ...user,
    }
  }
  return undefined
}
