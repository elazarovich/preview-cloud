const STATUS = {
  OPEN: "open",
  ACCEPTED: "accepted",
  COMPLETED: "completed",
  CANCELLED: "cancelled",
  EXPIRED: "expired",
}
export const OFFER_STATUS = STATUS
export const OFFER_REQUEST_STATUS = STATUS

export const APPROVED_STATUS = {
  IS_APPROVED: "isApproved",
  HAS_MADE_APPROVED_REQUESTS: "hasMadeApprovedRequest",
}
