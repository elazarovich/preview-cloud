import _ from "lodash"
import { REF_NAMES } from "../RefNames"
import { CategoryDB } from "helpdor-types"
import admin from "firebase-admin"
const { CATEGORIES } = REF_NAMES

interface Category extends CategoryDB {
  id: string
}

export const getCategory = async (
  database: admin.database.Database,
  categoryId: string,
): Promise<Category> => {
  const categorySnapshot = await database
    .ref(`/${CATEGORIES}`)
    .child(categoryId)
    .once("value")
  // tslint:disable-next-line:no-unsafe-any
  const category: CategoryDB = await categorySnapshot.val()
  return {
    ...category,
    id: categoryId,
  }
}

export const getName = async (
  database: admin.database.Database,
  categoryId: string,
) => {
  const category = await getCategory(database, categoryId)
  return category.name
}

export const getLocalizedName = (
  name: CategoryDB["name"],
  locale: string,
): string => {
  if (locale === "fi") {
    return name.fi
  } else {
    return name.en
  }
}
