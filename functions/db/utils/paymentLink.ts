import * as functions from "firebase-functions"

// tslint:disable-next-line:no-unsafe-any
const apiUrl: string = functions.config().hosting.url

export function getPaymentLink(
  offerRequestId: string,
  transactionId: string,
  offerId?: string,
): string {
  let link = `https://${apiUrl}/payment/${offerRequestId}/${transactionId}`
  if (offerId) {
    link += `/${offerId}`
  }
  return link
}
