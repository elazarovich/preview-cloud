import { getPaymentLink } from "./paymentLink"
import {
  OrderNotificationData,
  PrepayNotificationData,
} from "./types/NotificationsTypes"
import { Locales } from "../Translation"

function getPrepaySMSEN(prepayLink: string): string {
  return `Thanks for ordering via Helpdor. You can pay using bank debit/credit card using the following link: ${prepayLink}`
}

function getPrepaySMSFI(prepayLink: string): string {
  return `Kiitos että valitsit Helpdorin! Voit maksaa tilauksesi pankki- tai luottokortilla käyttämällä tätä linkkiä: ${prepayLink}`
}

export function getPrepaySMS(
  prepayData: PrepayNotificationData,
  locale: Locales,
): string {
  const { offerRequestId, transactionId, offerId } = prepayData
  const prepayLink = getPaymentLink(offerRequestId, transactionId, offerId)
  return locale === Locales.FI
    ? getPrepaySMSFI(prepayLink)
    : getPrepaySMSEN(prepayLink)
}

function getOfferOrderSMSEN(orderData: OrderNotificationData): string {
  const {
    companyName,
    companyEmail,
    companyVAT,
    categoryName,
    rating,
  } = orderData

  let sellerProfile = `Name: ${companyName}, Email: ${companyEmail}, Company VAT: ${companyVAT}`
  if (rating) {
    sellerProfile += `, Rating: ${rating}`
  }
  return `Thanks for ordering (${categoryName}) via Helpdor. This is to let you know that the service you’ve booked will be serviced by: ${sellerProfile}`
}

function getOfferOrderSMSFI(orderData: OrderNotificationData): string {
  const {
    companyName,
    companyEmail,
    companyVAT,
    categoryName,
    rating,
  } = orderData

  let sellerProfile = `Nimi: ${companyName}, Sähköposti: ${companyEmail}, Y-tunnus: ${companyVAT}`
  if (rating) {
    sellerProfile += `, Arvio: ${rating}`
  }
  return `Kiitos että valitsit (${categoryName}) Helpdorin! Teidän tilaamanne palvelun hoitaa: ${sellerProfile}`
}

export function getOfferOrderSMS(
  orderData: OrderNotificationData,
  locale: Locales,
): string {
  return locale === Locales.FI
    ? getOfferOrderSMSFI(orderData)
    : getOfferOrderSMSEN(orderData)
}
