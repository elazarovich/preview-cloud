import { getPaymentLink } from "./paymentLink"
import {
  OrderNotificationData,
  PrepayNotificationData,
} from "./types/NotificationsTypes"
import { Locales } from "../Translation"

function getPrepayEmailHTMLFI(prepayLink: string): string {
  return `<!DOCTYPE html>
    <html>
        <body>
            <p>Kiitos Tilauksesta!</p>
            <br />
            <p>Voit maksaa kayttaen seuraavia maksutapoja::</p>
            <p>Netti/tilisiirto</p>
            <p>Korttimaksu:</p>
            <a href="${prepayLink}">${prepayLink}</a>
            <br />
            <p>Toivottavasti pääsemme auttamaan sinua pian uudelleen!</p>
            <br />
            <p>Kiitos,</p>
            <p>Helpdor Oy</p>
            <p>Puh: 010 206 3230</p>
            <p>Email: info@helpdor.com</p>
        </body>
    </html>`
}

function getPrepayEmailHTMLEN(prepayLink: string): string {
  return `<!DOCTYPE html>
    <html>
        <body>
            <p>Thanks for ordering via Helpdor!</p>
            <br />
            <p>You can pay using the following methods:</p>
            <p>Bank credit/debit card</p>
            <p>Payment link:</p>
            <a href="${prepayLink}">${prepayLink}</a>
            <br/>
            <p>We look forward to being of service again soon!</p>
            <br/>
            <p>Thanks,</p>
            <p>Helpdor Oy</p>
            <p>Tel: 010 206 3230</p>
            <p>Email: info@helpdor.com</p>
        </body>
    </html>`
}

export function getPrepayEmailHTML(
  prepayData: PrepayNotificationData,
  locale: Locales,
): string {
  const { offerRequestId, transactionId, offerId } = prepayData
  const prepayLink = getPaymentLink(offerRequestId, transactionId, offerId)
  return locale === Locales.FI
    ? getPrepayEmailHTMLFI(prepayLink)
    : getPrepayEmailHTMLEN(prepayLink)
}

function getOfferOrderEmailHTMLEN(orderData: OrderNotificationData): string {
  const {
    categoryName,
    companyName,
    companyEmail,
    companyVAT,
    rating,
  } = orderData

  return `<!DOCTYPE html>
    <html>
      <body>
        <p><b>Our valued customer, Thank you for your order.</b></p>
        <br />
        <p>This is to let you know that the service (${categoryName ||
          ""}) you’ve booked will be serviced by: ${companyName ||
    ""}, ${companyEmail || ""}</p>
  <br />
        <p>You can find their registered company number here: ${companyVAT ||
          ""}</p>
        <br />
        <p>Their average rating on Helpdor platorm: ${rating || 0}</p>
        <br />
        <p>Be reassured that your booking is in good hands because at Helpdor we only choose to work with reliable & skilled professionals.</p>
        <br />
        <p>If you would like to amend your order or change the service provider please contact our Customer Service team at: Tel: 010 206 3230, Email: info@helpdor.com</p>
      </body>
    </html>`
}

function getOfferOrderEmailHTMLFI(orderData: OrderNotificationData): string {
  const {
    categoryName,
    companyName,
    companyEmail,
    companyVAT,
    rating,
  } = orderData

  return `<!DOCTYPE html>
    <html>
      <body>
        <p><b>Arvoisa Helpdorin asiakas.</b></p>
        <br />
        <p>Kiitos tilauksestanne. Teidän tilaamanne (${categoryName ||
          ""}) palvelun hoitaa: ${companyName || ""}, ${companyEmail || ""}</p>
  <br />
        <p>Y-tunnuksen löydät tästä: ${companyVAT || ""}</p>
        <br />
        <p>Tekijän arvostelut: ${rating || 0}</p>
        <br />
        <p>Tilaamanne palvelu on hyvissä käsissä sillä Helpdor valitsee ainoastaan luotettavia ja ammattitaitoisia tekijöitä/toimijoita.</p>
        <br />
        <p>Jos haluatte tehdä muutoksia tilaukseenne tai vaihtaa tekijää/toimijaa ottakaa yhteyttä Helpdorin asiakaspalveluun: Puh: 010 206 3230, Email: info@helpdor.com<</p>
      </body>
    </html>`
}

export function getOfferOrderEmailHTML(
  orderData: OrderNotificationData,
  locale: Locales,
): string {
  return locale === Locales.FI
    ? getOfferOrderEmailHTMLFI(orderData)
    : getOfferOrderEmailHTMLEN(orderData)
}
