export interface PrepayNotificationData {
  offerRequestId: string
  transactionId: string
  offerId?: string
}

export interface OrderNotificationData {
  companyName: string
  companyEmail: string
  companyVAT: string
  categoryName: string
  rating?: number
}
