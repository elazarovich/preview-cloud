import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { APPROVED_STATUS, OFFER_REQUEST_STATUS, OFFER_STATUS } from "../Status"
import { REF_NAMES } from "../RefNames"
import { OfferRequestDB } from "helpdor-types"
import { broadcastOfferRequestNotification } from "./utils/offerRequestNotification"

const { OFFER_REQUESTS, OFFERS, OPEN_OFFER_REQUESTS } = REF_NAMES
const { IS_APPROVED } = APPROVED_STATUS

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const cancelOffers = async (offers: OfferRequestDB["offers"]) => {
  if (!offers) {
    return undefined
  }
  const offersRef = admin
    .app()
    .database()
    .ref(`${OFFERS}`)
  return Promise.all(
    Object.keys(offers).map(id =>
      offersRef
        .child(id)
        .child("status")
        .set(OFFER_STATUS.CANCELLED),
    ),
  )
}

const expireOffers = async (offers: OfferRequestDB["offers"]) => {
  if (!offers) {
    return undefined
  }
  const offersRef = admin
    .app()
    .database()
    .ref(`${OFFERS}`)
  return Promise.all(
    Object.keys(offers).map(id =>
      offersRef
        .child(id)
        .child("status")
        .set(OFFER_STATUS.EXPIRED),
    ),
  )
}

const getOfferRequest = async (
  change: functions.Change<functions.database.DataSnapshot>,
): Promise<OfferRequestDB | undefined> => {
  const parent = change.after.ref.parent
  if (parent) {
    const offerRequestRef = await parent.once("value")
    // tslint:disable-next-line:no-unsafe-any
    return offerRequestRef.val()
  }
  return undefined
}

export const updateOffersStatus = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/status`)
  .onUpdate(async (change, context) => {
    const offerRequest = await getOfferRequest(change)
    if (offerRequest) {
      const { status, offers } = offerRequest
      if (status === OFFER_REQUEST_STATUS.CANCELLED) {
        // Cancels Offers, when the OfferRequest is cancelled
        return cancelOffers(offers)
      }
      if (status === OFFER_REQUEST_STATUS.EXPIRED) {
        // Expires Offers, when the OfferRequest is expired
        return expireOffers(offers)
      }
    }
    return undefined
  })

export const updateOpenRequestList = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/status`)
  .onUpdate(async (change, context) => {
    const offerRequest = await getOfferRequest(change)
    if (offerRequest) {
      const { status, isApproved } = offerRequest
      const openOfferRequestsRef = admin
        .app()
        .database()
        .ref(`${OPEN_OFFER_REQUESTS}`)
      if (isApproved && status === OFFER_REQUEST_STATUS.OPEN) {
        return openOfferRequestsRef
          .child(context.params.pushId as string)
          .set(true)
      }
      return openOfferRequestsRef
        .child(context.params.pushId as string)
        .set(null)
    }
  })

export const updateOpenRequestIfApproved = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/${IS_APPROVED}`)
  .onWrite((change, context) => updateOpenRequestList(change, context))

export const updatePrepaidStatus = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/prepaid`)
  .onUpdate((change, context) =>
    broadcastOfferRequestNotification(change.after, context),
  )
