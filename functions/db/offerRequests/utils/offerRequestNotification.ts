import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { BuyerProfileDB } from "helpdor-types/buyerProfile"
import { getAllSellersForCategory, User } from "../../users"
import { PushNotificationMeta } from "helpdor-types/notification"
import { getLocalizedName, getName } from "../../categories"
import {
  NotificationMessage,
  saveNotificationToDB,
  sendPushNotification,
} from "../../../notifications"
import { OfferRequestDB } from "helpdor-types/offerRequest"
import { REF_NAMES } from "../../RefNames"
import { CategoryDB } from "helpdor-types/category"
import { Translate } from "../../Translation"

const { BUYER_PROFILES } = REF_NAMES

export const getOfferRequest = async (
  snap: functions.database.DataSnapshot,
) => {
  if (!snap.ref.parent) {
    return undefined
  }
  const offerRequestRef = await snap.ref.parent.once("value")
  return offerRequestRef.val() as OfferRequestDB
}

export const composeNotifications = (
  users: User[],
  categoryName: CategoryDB["name"],
  buyerName: string,
  meta: PushNotificationMeta,
): NotificationMessage[] => {
  return users
    .filter(user => !user.isBlocked && user.isApprovedSeller && user.pushToken)
    .map(
      (user): NotificationMessage => {
        const userLanguage = user.language
        const title = getLocalizedName(categoryName, userLanguage)
        const messageBody = `${Translate(
          userLanguage,
          "new_offer_request_from",
        )} ${buyerName}`
        return {
          userId: user.id,
          to: user.pushToken
            ? `ExponentPushToken[${Object.keys(user.pushToken)[0]}]`
            : "",
          title,
          body: messageBody,
          data: {
            message: {
              title,
              body: messageBody,
            },
            meta,
          },
        }
      },
    )
}

export const broadcastOfferRequestNotification = async (
  snap: functions.database.DataSnapshot,
  context: functions.EventContext,
) => {
  const offerRequest = await getOfferRequest(snap)
  if (
    !offerRequest ||
    !offerRequest.isApproved ||
    (offerRequest.prepaid && offerRequest.prepaid !== "paid")
  ) {
    return
  }

  const database = admin.app().database()
  const buyerProfileSnapshot = await database
    .ref(`/${BUYER_PROFILES}`)
    .child(offerRequest.buyerProfile)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const { displayName }: BuyerProfileDB = buyerProfileSnapshot.val()
  const sellerUsers = await getAllSellersForCategory(
    database,
    offerRequest.category,
  )
  const users = sellerUsers.filter(
    user => user.id !== offerRequest.buyerProfile,
  )

  const meta: PushNotificationMeta = {
    type: "newOfferRequest",
    offerRequestId: context.params.pushId as string,
  }
  const categoryName = await getName(database, offerRequest.category)
  const notifications = composeNotifications(
    users,
    categoryName,
    displayName || "",
    meta,
  )
  await saveNotificationToDB(notifications)
  await sendPushNotification(notifications)
}
