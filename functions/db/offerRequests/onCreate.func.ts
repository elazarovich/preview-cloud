import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { sendReportEmail, sendSupportEmail } from "../../emails"
import { REF_NAMES } from "../RefNames"
import { APPROVED_STATUS } from "../Status"
import verifyOfferRequestTemplate from "./verifyOfferRequestTemplate"
import { BuyerProfileDB, OfferRequestDB, UserDB } from "helpdor-types"
import { sendSMS } from "../../sms"
import createTransaction from "../../https/transactions/utils/createTransaction"
import {
  broadcastOfferRequestNotification,
  getOfferRequest,
} from "./utils/offerRequestNotification"
import { Locales } from "../Translation"
import { getPrepaySMS } from "../utils/smsNotifications"
import { getPrepayEmailHTML } from "../utils/emailNotifications"
import { PrepayNotificationData } from "../utils/types/NotificationsTypes"

const { OFFER_REQUESTS, BUYER_PROFILES, USERS } = REF_NAMES
const { IS_APPROVED, HAS_MADE_APPROVED_REQUESTS } = APPROVED_STATUS

// tslint:disable-next-line:no-unsafe-any
const hostingUrl = functions.config().hosting.url

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const broadcastOfferRequest = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/${IS_APPROVED}`)
  .onCreate((snap, context) => broadcastOfferRequestNotification(snap, context))

export const approveBuyer = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/${IS_APPROVED}`)
  .onCreate(async (snap, context) => {
    const offerRequest = await getOfferRequest(snap)
    if (!offerRequest || !offerRequest.isApproved) {
      return
    }

    await admin
      .app()
      .database()
      .ref(`/${BUYER_PROFILES}`)
      .child(offerRequest.buyerProfile)
      .child(HAS_MADE_APPROVED_REQUESTS)
      .set(true)
  })

const sendApproveLink = async (props: {
  offerRequestId: string
  offerRequest: OfferRequestDB
  databaseInstance: admin.database.Database
}) => {
  const { addresses, buyerProfile, questions } = props.offerRequest
  const userSnapshot = await props.databaseInstance
    .ref(`/${USERS}`)
    .child(buyerProfile)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const { name, email }: UserDB = userSnapshot.val()
  const mailToList = [
    "janne@helpdor.com",
    "nish@helpdor.com",
    "juho@helpdor.com",
    "kalle@helpdor.com",
  ]
  const linkUrl = `${hostingUrl}/offerRequest/confirm?reqId=${props.offerRequestId}`

  return sendReportEmail({
    to: mailToList,
    subject: "New Offer Request",
    html: verifyOfferRequestTemplate({
      questions,
      addresses,
      name,
      email,
      linkUrl,
    }),
  })
}

export const screenNewRequest = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}`)
  .onCreate(async (snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const offerRequest: OfferRequestDB = await snap.val()

    const databaseInstance = admin.app().database()
    const buyerProfileSnapshot = await databaseInstance
      .ref(`/${BUYER_PROFILES}`)
      .child(offerRequest.buyerProfile)
      .once("value")

    // tslint:disable-next-line:no-unsafe-any
    const buyerProfile: BuyerProfileDB = buyerProfileSnapshot.val()

    if (buyerProfile.hasMadeApprovedRequest || offerRequest.prepaid) {
      return snap.ref.child(IS_APPROVED).set(true)
    }

    return sendApproveLink({
      databaseInstance,
      offerRequestId: context.params.pushId as string,
      offerRequest,
    })
  })

const sendPrepayMessage = async (
  snap: functions.database.DataSnapshot,
  context: functions.EventContext,
) => {
  const offerRequest = await getOfferRequest(snap)
  if (!offerRequest || !offerRequest.prePayWith) {
    return
  }
  const databaseInstance = admin.app().database()
  const buyerSnapshot = await databaseInstance
    .ref(`/${USERS}`)
    .child(offerRequest.buyerProfile)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const buyer: UserDB = buyerSnapshot.val()
  const offerRequestId = context.params.pushId as string
  const { transactionId } = await createTransaction({
    databaseInstance,
    offerRequestId,
    isExtending: false,
    buyerInformation: {
      buyerId: offerRequest.buyerProfile,
      email: buyer.email,
      name: buyer.name,
      userLocale: buyer.language,
    },
  })

  const prepayNotificationData: PrepayNotificationData = {
    offerRequestId,
    transactionId,
  }
  if (offerRequest.sendInvoiceWith === "sms") {
    await sendSMS(
      buyer.phone,
      getPrepaySMS(prepayNotificationData, buyer.language as Locales),
    )
  } else {
    await sendSupportEmail({
      to: buyer.email,
      subject:
        buyer.language === "fi"
          ? "Maksutavat - Kiitos Tilauksesta!"
          : "Payment - Thanks for ordering via Helpdor!",
      html: getPrepayEmailHTML(
        prepayNotificationData,
        buyer.language as Locales,
      ),
    })
  }
}

export const sendEmailNotification = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/prePayWith`)
  .onCreate((snap, context) => sendPrepayMessage(snap, context))
