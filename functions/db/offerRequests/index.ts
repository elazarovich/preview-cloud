import { REF_NAMES } from "../RefNames"
import admin = require("firebase-admin")
import { OfferRequestDB } from "helpdor-types"

const { OFFER_REQUESTS } = REF_NAMES

interface OfferRequest extends OfferRequestDB {
  id: string
}

export const getOfferRequest = async (
  database: admin.database.Database,
  id: string,
): Promise<OfferRequest | undefined> => {
  const offerRequestSnapshot = await database
    .ref(`/${OFFER_REQUESTS}`)
    .child(id)
    .once("value")
  const offerRequest: OfferRequestDB = offerRequestSnapshot.val() // tslint:disable-line:no-unsafe-any
  if (offerRequest && offerRequestSnapshot.key) {
    return {
      id: offerRequestSnapshot.key,
      ...offerRequest,
    }
  }
  return undefined
}

export const getOfferRequests = (
  database: admin.database.Database,
  ids: string[],
) => Promise.all(ids.map(id => getOfferRequest(database, id)))
