import _ from "lodash"
import { OfferRequestDB, BuyerProfileDB } from "helpdor-types"

const getUserInfo = (name: string, email: string) => `
    <div width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
        <p>Name: <b>${name}</b></p>
        <p>Email: <b>${email}</b></p>
    </div>`

const getAdddresses = (address: OfferRequestDB["addresses"]) => {
  const addressFields =
    address.type === "basic" ? address.main : address.from || address.to
  if (addressFields) {
    return `<div width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
            <p>Kaupunki: <b>${addressFields.city}</b></p>
            <p>Katuosoite: <b>${addressFields.streetAddress}</b></p>
            <p>Postinumero: <b>${addressFields.postalCode}</b></p>
        </div>`
  }
  return ""
}

const getLink = (linkUrl: string) =>
  `<p><a href="${linkUrl}" target="_top">Approve offer-request</a></p>`

interface TemplateProps {
  questions: OfferRequestDB["questions"]
  addresses: OfferRequestDB["addresses"]
  name: BuyerProfileDB["displayName"]
  email: string
  linkUrl: string
}

export default ({
  questions,
  addresses,
  name,
  email,
  linkUrl,
}: TemplateProps) => `<!DOCTYPE html>
    <html>
        <body>
            <p><b>New offer request:</b></p>
            ${getUserInfo(name || "", email)}
            <br/><br/>
            ${getAdddresses(addresses)}
            <br/><br/>
            ${getLink(linkUrl)}
        </body>
    </html>`
