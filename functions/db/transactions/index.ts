import { REF_NAMES } from "../RefNames"
import admin = require("firebase-admin")
import { TransactionDB } from "helpdor-types"

const { TRANSACTIONS } = REF_NAMES

interface Transaction extends TransactionDB {
  id: string
}

export const getTransaction = async (
  database: admin.database.Database,
  id: string,
): Promise<Transaction | undefined> => {
  const transactionSnapshot = await database
    .ref(`/${TRANSACTIONS}`)
    .child(id)
    .once("value")
  const transaction: TransactionDB = transactionSnapshot.val() // tslint:disable-line:no-unsafe-any
  if (transactionSnapshot.key) {
    return {
      id: transactionSnapshot.key,
      ...transaction,
    }
  }
  return undefined
}
