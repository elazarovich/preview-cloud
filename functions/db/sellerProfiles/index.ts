import * as admin from "firebase-admin"
import { SellerProfileDB } from "helpdor-types/sellerProfile"
import { REF_NAMES } from "../RefNames"

const { SELLER_PROFILES } = REF_NAMES

export const getSellerProfile: (
  databaseInstance: admin.database.Database,
  userIds: string,
) => Promise<SellerProfileDB> = async (databaseInstance, sellerProfileId) => {
  const sellerProfileSnapshot = await databaseInstance
    .ref(`/${SELLER_PROFILES}`)
    .child(sellerProfileId)
    .once("value")

  return sellerProfileSnapshot.val() as SellerProfileDB
}
