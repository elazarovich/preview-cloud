import { REF_NAMES } from "../RefNames"
import admin = require("firebase-admin")
import { OfferDB } from "helpdor-types"

const { OFFERS } = REF_NAMES

export interface Offer extends OfferDB {
  id: string
}

export const getOffer = async (
  database: admin.database.Database,
  id: string,
): Promise<Offer | undefined> => {
  const offerSnapshot = await database
    .ref(`/${OFFERS}`)
    .child(id)
    .once("value")
  const offer: OfferDB = offerSnapshot.val() // tslint:disable-line:no-unsafe-any
  if (offerSnapshot.key) {
    return {
      id: offerSnapshot.key,
      ...offer,
    }
  }
  return undefined
}
