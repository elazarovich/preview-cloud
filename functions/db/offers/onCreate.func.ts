import _ from "lodash"
import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { REF_NAMES } from "../RefNames"
import { getUser } from "../users"
import { getLocalizedName, getName } from "../categories"
import { Locales, Translate } from "../Translation"
import { getOfferRequest } from "../offerRequests"
import {
  OfferDB,
  PushNotificationMeta,
  ReviewDB,
  SellerProfileDB,
  UserDB,
} from "helpdor-types"
import { getSellerProfile } from "../sellerProfiles"
import { sendSupportEmail } from "../../emails"
import { sendSMS } from "../../sms"
import { getOfferOrderSMS } from "../utils/smsNotifications"
import { OrderNotificationData } from "../utils/types/NotificationsTypes"
import { getOfferOrderEmailHTML } from "../utils/emailNotifications"

const { OFFER_REQUESTS, SELLER_PROFILES, OFFERS, REVIEWS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const composeNotifications = async (
  buyer: UserDB,
  meta: PushNotificationMeta,
  offerRequestId: string,
  sellerId: string,
  databaseInstance: admin.database.Database,
) => {
  if (!buyer.pushToken) {
    return []
  }

  const sellerProfileSnapshot = await databaseInstance
    .ref(`/${SELLER_PROFILES}`)
    .child(sellerId)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const sellerProfile: SellerProfileDB = sellerProfileSnapshot.val()

  const offerRequest = await getOfferRequest(databaseInstance, offerRequestId)
  if (!offerRequest) {
    return undefined
  }

  const userLanguage = buyer.language
  const name = await getName(databaseInstance, offerRequest.category)
  const categoryName = getLocalizedName(name, userLanguage)
  const messageBody = `${Translate(userLanguage, "new_offer_from")} ${
    sellerProfile.companyName
  }`

  return [
    {
      userId: offerRequest.buyerProfile,
      to: `ExponentPushToken[${Object.keys(buyer.pushToken)[0]}]`,
      title: categoryName,
      body: messageBody,
      data: {
        message: {
          title: categoryName,
          body: messageBody,
        },
        meta,
      },
    },
  ]
}

const getSellerRating = async (
  sellerProfileId: string,
): Promise<number | null> => {
  const databaseInstance = admin.app().database()
  const sellerReviewsSnapshot = await databaseInstance
    .ref(`/${SELLER_PROFILES}`)
    .child(sellerProfileId)
    .child(REVIEWS)
    .once("value")

  const sellerReviews: {
    [id: string]: true
  } | null = sellerReviewsSnapshot.val()

  if (sellerReviews === null) {
    return null
  }

  const reviewsIds = Object.keys(sellerReviews)
  const ratings: number[] = []
  for (let i = 0; i < reviewsIds.length; i++) {
    const reviewId = reviewsIds[i]
    const reviewSnapshot = await databaseInstance
      .ref(`/${REVIEWS}`)
      .child(reviewId)
      .once("value")
    const { rating }: ReviewDB = reviewSnapshot.val()
    ratings.push(rating)
  }
  return Math.round((_.sum(ratings) / ratings.length) * 10) / 10
}

export const sendEmailNotificationForOffersFromStilaus = functions.database
  .ref(`/${OFFERS}/{pushId}`)
  .onCreate(async (snap, context) => {
    const databaseInstance = admin.app().database()
    const offer: OfferDB = snap.val()
    const offerRequest = await getOfferRequest(
      databaseInstance,
      offer.offerRequest,
    )

    if (!(offerRequest && offerRequest.prepaid)) {
      return
    }
    const {
      buyerProfile: buyerProfileId,
      sellerProfile: sellerProfileId,
    } = offer
    const sellerUser = await getUser(databaseInstance, sellerProfileId)
    const buyerUser = await getUser(databaseInstance, buyerProfileId)
    const sellerProfile = await getSellerProfile(
      databaseInstance,
      sellerProfileId,
    )
    const userLanguage = buyerUser.language
    const name = await getName(databaseInstance, offerRequest.category)
    const categoryName = getLocalizedName(name, userLanguage)

    if (!(sellerUser && sellerProfile && buyerUser)) {
      return
    }

    const rating = await getSellerRating(sellerProfileId)
    const orderData: OrderNotificationData = {
      categoryName,
      rating,
      companyName: sellerUser.name,
      companyEmail: sellerUser.email,
      companyVAT: sellerProfile.companyVAT,
    }
    if (offerRequest.sendInvoiceWith === "sms") {
      await sendSMS(
        buyerUser.phone,
        getOfferOrderSMS(orderData, buyerUser.language as Locales),
      )
      return
    }

    const html = getOfferOrderEmailHTML(
      orderData,
      buyerUser.language as Locales,
    )
    const subject =
      buyerUser.language === Locales.FI
        ? "Kiitos Tilauksesta!"
        : "Thank you for your order!"

    await sendSupportEmail({
      html,
      subject,
      to: buyerUser.email,
    })
  })

// When new Offer is created, send a notification about it
export const sendOfferNotification = functions.database
  .ref(`/${OFFERS}/{pushId}`)
  .onCreate(async (snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = snap.val()

    const {
      buyerProfile: buyerId,
      sellerProfile: sellerId,
      offerRequest,
    } = offer

    const databaseInstance = admin.app().database()

    const buyer = await getUser(databaseInstance, buyerId)

    const notifications = await composeNotifications(
      buyer,
      {
        type: "newOffer",
        offerRequestId: offerRequest,
        offerId: context.params.pushId as string,
      },
      offerRequest,
      sellerId,
      databaseInstance,
    )
    if (notifications) {
      await saveNotificationToDB(notifications)
      await sendPushNotification(notifications)
    }
  })

// When new Offer is created, updates the OfferRequest.offers with a reference to that Offer
export const addOfferToOfferRequest = functions.database
  .ref(`/${OFFERS}/{pushId}`)
  .onCreate(async (snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = snap.val()
    const { offerRequest } = offer
    return admin
      .app()
      .database()
      .ref(`/${OFFER_REQUESTS}`)
      .child(offerRequest)
      .child("offers")
      .child(context.params.pushId as string)
      .set(true)
  })
