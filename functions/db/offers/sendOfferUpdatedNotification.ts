import * as admin from "firebase-admin"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import {
  OFFER_COMPLETED,
  OFFER_STARTED,
  OFFER_PAUSED,
  OFFER_CONTINUED,
  OFFER_EXTEND_REQUESTED,
} from "../NotificationTypes"
import { REF_NAMES } from "../RefNames"
import { Translate } from "../Translation"
import { getUser } from "../users"
import { getLocalizedName, getName } from "../categories"
import { getOfferRequest } from "../offerRequests"
import { PushNotificationMeta, OfferDB, SellerProfileDB } from "helpdor-types"

const { SELLER_PROFILES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

type NotificationTypes =
  | "offerStarted"
  | "offerPaused"
  | "offerContinued"
  | "offerCompleted"
  | "offerExtendRequested"

const getMessageBody = (
  name: string,
  userLanguage: string,
  notificationType: NotificationTypes,
) => {
  let body = ""
  switch (notificationType) {
    case OFFER_STARTED:
      body = `${name} ${Translate(userLanguage, "started_the_job")}`
      break
    case OFFER_PAUSED:
      body = `${name} ${Translate(userLanguage, "paused_the_job")}`
      break
    case OFFER_CONTINUED:
      body = `${name} ${Translate(userLanguage, "continued_the_job")}`
      break
    case OFFER_COMPLETED:
      body = `${name} ${Translate(userLanguage, "completed_the_job")}`
      break
    case OFFER_EXTEND_REQUESTED:
      body = `${name} ${Translate(userLanguage, "requested_to_extend")}`
      break
    default:
      break
  }
  return body
}

const offerUpdatedNotification = async (
  offer: OfferDB & { id: string },
  notificationType: NotificationTypes,
) => {
  const {
    buyerProfile: buyerId,
    sellerProfile: sellerId,
    offerRequest: offerRequestId,
  } = offer
  const database = admin.app().database()

  const buyer = await getUser(database, buyerId)
  if (!buyer.pushToken) {
    return []
  }

  const userLanguage = buyer.language

  const offerRequest = await getOfferRequest(database, offerRequestId)
  if (!offerRequest) {
    return undefined
  }

  const { category } = offerRequest

  const sellerProfileSnapshot = await database
    .ref(`/${SELLER_PROFILES}`)
    .child(sellerId)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const { companyName }: SellerProfileDB = sellerProfileSnapshot.val()

  const categoryNameObject = await getName(database, category)
  const categoryName = getLocalizedName(categoryNameObject, userLanguage)
  const body = getMessageBody(companyName, userLanguage, notificationType)
  const meta: PushNotificationMeta = {
    type: notificationType,
    offerId: offer.id,
    offerRequestId,
  }
  return [
    {
      userId: buyerId,
      to: `ExponentPushToken[${Object.keys(buyer.pushToken)[0]}]`,
      title: categoryName,
      body,
      data: {
        message: {
          title: categoryName,
          body,
        },
        meta,
      },
    },
  ]
}

// Send Notification when the job is started
export default async (
  offer: OfferDB & { id: string },
  notificationType: NotificationTypes,
) => {
  const notifications = await offerUpdatedNotification(offer, notificationType)
  if (notifications) {
    await saveNotificationToDB(notifications)
    await sendPushNotification(notifications)
  }
}
