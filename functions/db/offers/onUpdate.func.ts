import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { OFFER_REQUEST_STATUS, OFFER_STATUS } from "../Status"
import { REF_NAMES } from "../RefNames"
import sendOfferAcceptedNotification from "./sendOfferAcceptedNotification"
import sendOfferUpdatedNotification from "./sendOfferUpdatedNotification"
import sendStartsInNotification from "./sendStartsInNotification"
import sendOfferExtendedNotification from "./sendOfferExtendedNotification"
import { OfferDB, OfferRequestDB, Purhase } from "helpdor-types"
import { getUser } from "../users"
import createTransaction from "../../https/transactions/utils/createTransaction"
import { sendSMS } from "../../sms"
import { sendSupportEmail } from "../../emails"
import { getOfferRequest } from "../offerRequests"
import { Locales } from "../Translation"
import { getPrepayEmailHTML } from "../utils/emailNotifications"
import { getPrepaySMS } from "../utils/smsNotifications"
import { PrepayNotificationData } from "../utils/types/NotificationsTypes"

const { OFFER_REQUESTS, OFFERS } = REF_NAMES

type Offer = OfferDB & { id: string }

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

// When an Offer is accepted, set OfferRequest also accepted
const setOfferRequestAccepted = async (offerRequestId: string) =>
  admin
    .app()
    .database()
    .ref(OFFER_REQUESTS)
    .child(offerRequestId)
    .child("status")
    .set(OFFER_REQUEST_STATUS.ACCEPTED)

// When an Offer is accepted, set other offers for the same offerRequest cancelled
const setOtherOffersCancelled = async (
  offerId: string,
  offerRequestId: string,
) => {
  const databaseRef = admin.app().database()
  const offerRequestSnapshot = await databaseRef
    .ref(OFFER_REQUESTS)
    .child(offerRequestId)
    .once("value")

  // tslint:disable-next-line:no-unsafe-any
  const offerRequest: OfferRequestDB | null = offerRequestSnapshot.val()
  if (offerRequest && offerRequest.offers) {
    return Promise.all(
      Object.keys(offerRequest.offers)
        .filter((id: string) => id !== offerId)
        .map((id: string) =>
          databaseRef
            .ref(OFFERS)
            .child(id)
            .child("status")
            .set(OFFER_STATUS.CANCELLED),
        ),
    )
  }
  return undefined
}

// When an Offer is completed, set OfferRequest also completed
const setOfferRequestCompleted = async (offerRequestId: string) =>
  admin
    .app()
    .database()
    .ref(OFFER_REQUESTS)
    .child(offerRequestId)
    .child("status")
    .set(OFFER_REQUEST_STATUS.COMPLETED)

const getOffer = async (
  change: functions.Change<functions.database.DataSnapshot>,
): Promise<Offer | undefined> => {
  if (change.after.ref.parent) {
    const offerRef = await change.after.ref.parent.once("value")
    const offer = offerRef.val()
    return offerRef.key
      ? {
          ...offer,
          id: offerRef.key,
        }
      : undefined
  }
  return undefined
}

const sendPausedNotification = async (offer: Offer) =>
  sendOfferUpdatedNotification(offer, "offerPaused")

// Send Notification when the job is paused
const sendContinuedNotification = async (offer: Offer) =>
  sendOfferUpdatedNotification(offer, "offerContinued")

// Send Notification when the job is completed
const sendCompletedNotification = async (offer: Offer) =>
  sendOfferUpdatedNotification(offer, "offerCompleted")

const updateOfferRequestStatusAndNotify = async (
  change: functions.Change<functions.database.DataSnapshot>,
) => {
  const offer = await getOffer(change)
  if (!offer) {
    return undefined
  }
  const { offerRequest: offerRequestId, status } = offer
  if (status === OFFER_STATUS.COMPLETED) {
    await setOfferRequestCompleted(offerRequestId)
    await sendCompletedNotification(offer)
  } else if (status === OFFER_STATUS.ACCEPTED) {
    await Promise.all([
      setOfferRequestAccepted(offerRequestId),
      setOtherOffersCancelled(offer.id, offerRequestId),
    ])
    return sendOfferAcceptedNotification(offer.id, offer)
  }
}

// Send Job starts in an hour notification
const sendStartsInHourNotification = async (
  change: functions.Change<functions.database.DataSnapshot>,
) => {
  const notify = change.after.val()
  const offer = await getOffer(change)
  if (notify && offer) {
    await sendStartsInNotification(offer, "job_stars_with_in_hour")
  }
}

const sendStartsInDayNotification = async (
  change: functions.Change<functions.database.DataSnapshot>,
) => {
  const notify = change.after.val()
  const offer = await getOffer(change)
  if (notify && offer) {
    await sendStartsInNotification(offer, "job_starts_tomorrow")
  }
}

const sendOfferUpdatedCommunicationNotification = async (
  databaseInstance: admin.database.Database,
  offer: Offer,
  type: "sms" | "email",
  transactionId: string,
) => {
  const { buyerProfile, offerRequest } = offer
  const buyer = await getUser(databaseInstance, buyerProfile)

  const prepayNotificationData: PrepayNotificationData = {
    transactionId,
    offerRequestId: offerRequest,
    offerId: offer.id,
  }
  if (type === "email") {
    return sendSupportEmail({
      to: buyer.email,
      subject:
        buyer.language === "fi"
          ? "Maksutavat - Kiitos Tilauksesta!"
          : "Payment - Thanks for ordering via Helpdor!",
      html: getPrepayEmailHTML(
        prepayNotificationData,
        buyer.language as Locales,
      ),
    })
  }

  return sendSMS(
    buyer.phone,
    getPrepaySMS(prepayNotificationData, buyer.language as Locales),
  )
}

const sendExtensionRequestedNotification = async (
  change: functions.Change<functions.database.DataSnapshot>,
) => {
  // tslint:disable-next-line:no-unsafe-any
  const timeToExtend: number = change.after.val()

  if (!timeToExtend || timeToExtend === 0) {
    return
  }

  const offer = await getOffer(change)
  if (!offer) {
    return
  }

  const database = admin.app().database()
  const buyer = await getUser(database, offer.buyerProfile)
  if (!buyer.pushToken) {
    const offerRequest = await getOfferRequest(database, offer.offerRequest)
    const { transactionId } = await createTransaction({
      databaseInstance: database,
      offerId: offer.id,
      offerRequestId: offerRequest.id,
      buyerInformation: {
        buyerId: buyer.id,
        email: buyer.email,
        name: buyer.name,
        userLocale: buyer.language,
      },
      isExtending: true,
    })
    await sendOfferUpdatedCommunicationNotification(
      database,
      offer,
      offerRequest.sendInvoiceWith,
      transactionId,
    )
    return
  }

  await sendOfferUpdatedNotification(offer, "offerExtendRequested")
}

export const updateOfferRequestStatus = functions.database
  .ref(`/${OFFERS}/{pushId}/status`)
  .onUpdate((change, context) => updateOfferRequestStatusAndNotify(change))

export const updateOfferRequestStartTime = functions.database
  .ref(`/${OFFERS}/{pushId}/actualStartTime`)
  .onUpdate(async (change, context) => {
    const offer = await getOffer(change)
    if (!offer) {
      return
    }
    const { offerRequest: offerRequestId, actualStartTime } = offer
    return admin
      .app()
      .database()
      .ref(OFFER_REQUESTS)
      .child(offerRequestId)
      .child("actualStartTime")
      .set(actualStartTime)
  })

export const sendStartedNotification = functions.database
  .ref(`/${OFFERS}/{pushId}/actualStartTime`)
  .onWrite(async (change, context) => {
    // Send Notification when the job is started
    const offer = await getOffer(change)
    if (offer) {
      await sendOfferUpdatedNotification(offer, "offerStarted")
    }
  })

export const sendPausedAndContinuedNotification = functions.database
  .ref(`/${OFFERS}/{pushId}/isPaused`)
  .onWrite(async (change, context) => {
    // Check the state of offer pause flag
    const offer = await getOffer(change)
    if (!offer || !offer.workTime) {
      return undefined
    }
    const { isPaused } = offer
    if (isPaused) {
      return sendPausedNotification(offer)
    } else {
      return sendContinuedNotification(offer)
    }
  })

export const sendHourLeftReminder = functions.database
  .ref(`/${OFFERS}/{pushId}/preHourReminder`)
  .onWrite((change, context) => sendStartsInHourNotification(change))

export const sendDayLeftReminder = functions.database
  .ref(`/${OFFERS}/{pushId}/preDayReminder`)
  .onWrite((change, context) => sendStartsInDayNotification(change))

export const sendExtensionNotification = functions.database
  .ref(`/${OFFERS}/{pushId}/timeToExtend`)
  .onWrite((change, context) => sendExtensionRequestedNotification(change))

export const sendExtendedNotification = functions.database
  .ref(`/${OFFERS}/{pushId}/extensions/{extension}`)
  .onCreate(async (snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const extenstion: Purhase = snap.val()
    const parent = snap.ref.parent?.parent
    if (!parent) {
      return undefined
    }
    const offerRef = await parent.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB | null = offerRef.val()
    if (
      offerRef.key &&
      offer &&
      extenstion &&
      extenstion.purchaseDuration > 0
    ) {
      return sendOfferExtendedNotification({
        ...offer,
        id: offerRef.key,
      })
    }
  })
