import * as admin from "firebase-admin"
import {
  saveNotificationToDB,
  sendPushNotification,
  NotificationMessage,
} from "../../notifications"
import { getUser } from "../users"
import { getLocalizedName, getName } from "../categories"
import { getOfferRequest } from "../offerRequests"
import { Translate } from "../Translation"
import { OFFER_ACCEPTED } from "../NotificationTypes"
import { PushNotificationMeta, OfferDB } from "helpdor-types"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

type Message = "job_stars_with_in_hour" | "job_starts_tomorrow"
type MessageForSeller = "seller_starts_in_hour" | "seller_starts_in_day"
type MessageForBuyer = "buyer_starts_in_hour" | "buyer_starts_in_day"

const getNotifications = async (
  notifications: NotificationMessage[],
  database: admin.database.Database,
  userId: string,
  message: MessageForSeller | MessageForBuyer,
  category: string,
  meta: PushNotificationMeta,
) => {
  const user = await getUser(database, userId)
  if (!user.pushToken) {
    return notifications
  }

  const userLanguage = user.language
  const categoryNameObject = await getName(database, category)
  const categoryName = getLocalizedName(categoryNameObject, userLanguage)

  const title = `${Translate(userLanguage, "reminder")}:`
  const body = `${categoryName} ${Translate(userLanguage, message)}`

  notifications.push({
    userId,
    to: `ExponentPushToken[${Object.keys(user.pushToken)[0]}]`,
    sound: "default",
    title,
    body,
    data: {
      message: {
        title,
        body,
      },
      meta,
    },
  })
  return notifications
}

const startsInNotifications = async (
  offer: OfferDB & { id: string },
  messageType: Message,
) => {
  const database = admin.app().database()

  const meta: PushNotificationMeta = {
    type: OFFER_ACCEPTED,
    offerId: offer.id,
    offerRequestId: offer.offerRequest,
  }

  const offerRequest = await getOfferRequest(database, offer.offerRequest)
  if (!offerRequest) {
    return undefined
  }

  const { category } = offerRequest

  const messageTypeForSeller =
    messageType === "job_stars_with_in_hour"
      ? "seller_starts_in_hour"
      : "seller_starts_in_day"

  let notifications = await getNotifications(
    [],
    database,
    offer.sellerProfile,
    messageTypeForSeller,
    category,
    meta,
  )

  const messageTypeForBuyer =
    messageType === "job_stars_with_in_hour"
      ? "buyer_starts_in_hour"
      : "buyer_starts_in_day"

  notifications = await getNotifications(
    notifications,
    database,
    offer.buyerProfile,
    messageTypeForBuyer,
    category,
    meta,
  )

  return notifications
}

export default async (offer: OfferDB & { id: string }, message: Message) => {
  const notifications = await startsInNotifications(offer, message)
  if (notifications) {
    await saveNotificationToDB(notifications)
    await sendPushNotification(notifications)
  }
}
