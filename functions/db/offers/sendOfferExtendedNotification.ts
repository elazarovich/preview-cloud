import * as admin from "firebase-admin"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { OFFER_EXTENDED } from "../NotificationTypes"
import { Translate } from "../Translation"
import { getUser } from "../users"
import { getLocalizedName, getName } from "../categories"
import { getOfferRequest } from "../offerRequests"
import { OfferDB, PushNotificationMeta } from "helpdor-types"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const offerExtendedNotification = async (offer: OfferDB & { id: string }) => {
  const {
    buyerProfile: buyerId,
    sellerProfile: sellerId,
    offerRequest: offerRequestId,
    timeToExtend,
  } = offer
  const database = admin.app().database()
  const offerRequest = await getOfferRequest(database, offerRequestId)
  const seller = await getUser(database, sellerId)

  if (!seller.pushToken || !offerRequest || !timeToExtend) {
    return undefined
  }
  const userLanguage = seller.language

  const { name } = await getUser(database, buyerId)

  const { category } = offerRequest

  const durationString =
    timeToExtend > 1
      ? `${timeToExtend} ${Translate(userLanguage, "hours")}`
      : `${timeToExtend} ${Translate(userLanguage, "hour")}`

  const categoryNameObject = await getName(database, category)
  const categoryName = getLocalizedName(categoryNameObject, userLanguage)
  const body = `${name} ${Translate(
    userLanguage,
    "extended_offer_duration",
  )} ${durationString}`
  const meta: PushNotificationMeta = {
    type: OFFER_EXTENDED,
    offerId: offer.id,
    offerRequestId,
  }
  return [
    {
      userId: sellerId,
      to: `ExponentPushToken[${Object.keys(seller.pushToken)[0]}]`,
      title: categoryName,
      body,
      data: {
        message: {
          title: categoryName,
          body,
        },
        meta,
      },
    },
  ]
}

// Send Notification when the job is started
export default async (offer: OfferDB & { id: string }) => {
  const notifications = await offerExtendedNotification(offer)
  if (notifications) {
    await saveNotificationToDB(notifications)
    await sendPushNotification(notifications)
  }
}
