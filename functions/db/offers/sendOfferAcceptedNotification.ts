import * as admin from "firebase-admin"
import { saveNotificationToDB, sendPushNotification } from "../../notifications"
import { REF_NAMES } from "../RefNames"
import { Translate } from "../Translation"
import { getUser } from "../users"
import { getLocalizedName, getName } from "../categories"
import { getOfferRequest } from "../offerRequests"
import { OfferDB, PushNotificationMeta } from "helpdor-types"

const { BUYER_PROFILES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

// When an Offer is accepted, send a notification about it
export default async (offerId: string, offer: OfferDB) => {
  const {
    offerRequest: offerRequestId,
    buyerProfile: buyerId,
    sellerProfile: sellerId,
  } = offer
  const database = admin.app().database()
  const offerRequest = await getOfferRequest(database, offerRequestId)
  const seller = await getUser(database, sellerId)
  if (!seller.pushToken || !offerRequest) {
    return
  }

  const userLanguage = seller.language

  const buyerProfileSnapshot = await database
    .ref(`/${BUYER_PROFILES}`)
    .child(buyerId)
    .once("value")
  const { displayName: buyerName } = buyerProfileSnapshot.val()

  const { category } = offerRequest

  const name = await getName(database, category)
  const categoryName = getLocalizedName(name, userLanguage)
  const messageBody = `${buyerName} ${Translate(
    userLanguage,
    "accepted_your_offer",
  )}`
  const meta: PushNotificationMeta = {
    type: "offerAccepted",
    offerId,
    offerRequestId: offerRequest.id,
  }
  const notifications = [
    {
      userId: sellerId,
      to: `ExponentPushToken[${Object.keys(seller.pushToken)[0]}]`,
      title: categoryName,
      body: messageBody,
      data: {
        message: {
          title: categoryName,
          body: messageBody,
        },
        meta,
      },
    },
  ]

  await saveNotificationToDB(notifications)
  await sendPushNotification(notifications)
}
