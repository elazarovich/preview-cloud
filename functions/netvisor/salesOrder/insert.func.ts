import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { getOfferRequest } from "../../db/offerRequests"
import { OfferDB, OfferRequestDB } from "helpdor-types"
import { saveSalesOrder } from "./insert"

const { OFFERS, OFFER_REQUESTS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const saveOffer = functions.database
  .ref(`/${OFFERS}/{pushId}/status`)
  .onWrite(async (change, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const previousStatus: OfferDB["status"] = change.before.val()
    // tslint:disable-next-line:no-unsafe-any
    const currentStatus: OfferDB["status"] = change.after.val()

    const offerSnapshot = await change.after.ref.parent.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = offerSnapshot.val()
    const database = admin.database()
    const offerRequest = await getOfferRequest(database, offer.offerRequest)
    if (
      currentStatus === "accepted" &&
      previousStatus !== currentStatus &&
      !offerRequest.prepaid &&
      !offerRequest.netvisorSalesOrderId
    ) {
      return saveSalesOrder({
        offerRequest,
        offer: {
          ...offer,
          id: context.params.pushId as string,
        },
        duration: offer.original.purchaseDuration,
        pricePerHour: offer.original.pricePerHour,
      })
    }
    return 0
  })

// Send Prepaid OfferRequests to Netvisor as SalesOrders
export const savePrepaidOfferRequest = functions.database
  .ref(`/${OFFER_REQUESTS}/{pushId}/prepaid`)
  .onCreate(async (snapshot, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const currentStatus: OfferRequestDB["prepaid"] = snapshot.val()
    if (currentStatus !== "waiting") {
      return 0
    }

    const offerRequestSnapshot = await snapshot.ref.parent.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const offerRequest: OfferRequestDB = offerRequestSnapshot.val()
    if (
      offerRequest.fixedDuration &&
      offerRequest.fixedPrice &&
      !offerRequest.netvisorSalesOrderId
    ) {
      return saveSalesOrder({
        offerRequest: {
          ...offerRequest,
          id: context.params.pushId as string,
        },
        duration: offerRequest.fixedDuration,
        pricePerHour: offerRequest.fixedPrice,
      })
    }
    return 0
  })
