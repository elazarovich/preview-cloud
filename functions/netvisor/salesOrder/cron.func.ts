import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { ErrorReporting } from "@google-cloud/error-reporting"
import { REF_NAMES } from "../../db/RefNames"
import { saveSalesOrder } from "./insert"
import { OfferDB, OfferRequestDB } from "helpdor-types"
import { getOfferRequest } from "../../db/offerRequests"

const { OFFERS, OFFER_REQUESTS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const errors = new ErrorReporting()

const sendAllOffersToNetvisorAsSalesOrders = async () => {
  const database = admin.database()
  const offerRequestsSnapshot = await database
    .ref(`${OFFER_REQUESTS}`)
    .once("value")

  const offerRequests: { [key: string]: OfferRequestDB } = {}
  offerRequestsSnapshot.forEach(offerRequestSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offerRequest: OfferRequestDB = offerRequestSnapshot.val()
    if (
      offerRequest.prepaid === "paid" &&
      offerRequest.fixedDuration &&
      offerRequest.fixedPrice &&
      offerRequest.netvisorSalesOrderId
    ) {
      offerRequest[offerRequestSnapshot.key] = offerRequest
    }
  })

  await Promise.all(
    Object.keys(offerRequests).map(key =>
      saveSalesOrder({
        offerRequest: {
          ...offerRequests[key],
          id: key,
        },
        duration: offerRequests[key].fixedDuration,
        pricePerHour: offerRequests[key].fixedPrice,
      }),
    ),
  )
}

const sendAllPrepaidOfferRequestsToNetvisorAsSalesOrders = async () => {
  const database = admin.database()
  const offersSnapshot = await database.ref(`${OFFERS}`).once("value")

  const offers: { [key: string]: OfferDB } = {}
  offersSnapshot.forEach(offerSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = offerSnapshot.val()
    if (offer.status === "accepted" && !offer.netvisorPurchaseOrderId) {
      offers[offerSnapshot.key] = offer
    }
  })

  await Promise.all(
    Object.keys(offers).map(async key => {
      const offerRequest = await getOfferRequest(
        database,
        offers[key].offerRequest,
      )
      if (!offerRequest) {
        const error = new Error(
          `OfferRequest ${offers[key].offerRequest} not found for Offer ${key}`,
        )
        errors.report(error)
        throw error
      } else if (offerRequest.prepaid) {
        return
      }
      return saveSalesOrder({
        offerRequest,
        offer: {
          ...offers[key],
          id: key,
        },
        duration: offers[key].original.purchaseDuration,
        pricePerHour: offers[key].original.pricePerHour,
      })
    }),
  )
}

export const pubsubOffers = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllOffersToNetvisorAsSalesOrders)
export const pubsubPrepaid = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllPrepaidOfferRequestsToNetvisorAsSalesOrders)

export const httpsOffers = functions.https.onRequest(async (req, res) => {
  await sendAllOffersToNetvisorAsSalesOrders()
  return res.send("done")
})
export const httpsPrepaid = functions.https.onRequest(async (req, res) => {
  await sendAllPrepaidOfferRequestsToNetvisorAsSalesOrders()
  return res.send("done")
})
