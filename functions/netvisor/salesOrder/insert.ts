import _ from "lodash"
import * as admin from "firebase-admin"
import { OfferRequestDB, UserDB, OfferDB, CategoryDB } from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"
import moment from "moment-timezone"
import { findOfferRequestDeadline } from "../../pubSub/index.func"
import { getUser } from "../../db/users"
import { getCategory } from "../../db/categories"
import { convertToDecimals } from "../helpers"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "salesinvoice.nv"

interface Props {
  offerRequest: OfferRequestDB & { id: string }
  offer?: OfferDB & { id: string }
  pricePerHour: number
  duration: number
}

interface XMLProps extends Props {
  deadline: number
  user: UserDB & Required<Pick<UserDB, "netvisorCustomerId">> & { id: string }
  category: CategoryDB &
    Required<Pick<CategoryDB, "netvisorKey">> & { id: string }
}

const getInvoiceLinesXML = (props: XMLProps) =>
  `<invoicelines>
    <invoiceline>
      <salesinvoiceproductline>
        <productidentifier type="netvisor">${
          props.category.netvisorKey
        }</productidentifier>
        <productname>${props.category.name.fi}</productname>
        <productunitprice type="gross">${convertToDecimals(
          props.pricePerHour,
        )}</productunitprice>
        <productvatpercentage vatcode="KOMY">22</productvatpercentage>
        <salesinvoiceproductlinequantity>${
          props.duration
        }</salesinvoiceproductlinequantity>
      </salesinvoiceproductline>
    </invoiceline>
  </invoicelines>`

const getSalesOrderXML = (props: XMLProps) => {
  return `<root>
    <salesinvoice>
      <salesinvoicedate>${moment().format("YYYY-MM-DD")}</salesinvoicedate>
      <salesinvoiceduedate format="ansi">${moment(props.deadline, "x").format(
        "YYYY-MM-DD",
      )}</salesinvoiceduedate>
      <salesinvoicedeliverytocustomerdate format="ansi" type="date">${moment(
        props.deadline,
        "x",
      ).format("YYYY-MM-DD")}</salesinvoicedeliverytocustomerdate>
      <salesinvoiceamount/>
      <invoicetype>order</invoicetype>
      <salesinvoicestatus type="netvisor">undelivered</salesinvoicestatus>
      <invoicingcustomeridentifier type="netvisor">${
        props.user.netvisorCustomerId
      }</invoicingcustomeridentifier>
      <invoicingcustomername>${props.user.name}</invoicingcustomername>
      <invoicingcustomercountrycode type="ISO-3316">FI</invoicingcustomercountrycode>
      <deliveryaddressname>${props.user.name}</deliveryaddressname>
      <deliveryaddressline>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.streetAddress
          : props.offerRequest.addresses.to.streetAddress
      }</deliveryaddressline>
      <deliveryaddresspostnumber>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.postalCode
          : props.offerRequest.addresses.to.postalCode
      }</deliveryaddresspostnumber>
      <deliveryaddresstown>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.city
          : props.offerRequest.addresses.to.city
      }</deliveryaddresstown>
      <deliveryaddresscountrycode type="ISO-3316">FI</deliveryaddresscountrycode>
      ${getInvoiceLinesXML(props)}
    </salesinvoice>
    </root>`
}

export const saveSalesOrder = async (props: Props) => {
  if (props.offerRequest.netvisorSalesOrderId) {
    console.log(
      `OfferRequest (${props.offerRequest.id}) already added to Netvisor with a id ${props.offerRequest.netvisorSalesOrderId}`,
    )
    return
  }

  const database = admin.app().database()

  const user = await getUser(database, props.offerRequest.buyerProfile)
  if (!user || user.netvisorCustomerId === undefined) {
    throw new Error("Invalid User")
  }

  const category = await getCategory(database, props.offerRequest.category)
  if (!category || category.netvisorKey === undefined) {
    throw new Error("Invalid Category")
  }

  const deadline = props.offer
    ? props.offer.agreedStartTime
    : findOfferRequestDeadline(props.offerRequest)

  const payload = getSalesOrderXML({
    user: {
      id: props.offerRequest.buyerProfile,
      netvisorCustomerId: user.netvisorCustomerId,
      ...user,
    },
    category: {
      id: props.offerRequest.category,
      netvisorKey: category.netvisorKey,
      ...category,
    },
    deadline,
    ...props,
  })

  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: `?method=add`,
    xml: payload,
  })
  // tslint:disable-next-line:no-unsafe-any
  const netvisorKey: string | undefined = _.get(
    responseData,
    "Root.Replies[0].InsertedDataIdentifier[0]",
  )

  if (responseData && netvisorKey) {
    const updatedOfferRequest: Partial<OfferRequestDB> = {
      netvisorSalesOrderId: netvisorKey,
    }
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.OFFER_REQUESTS}/${props.offerRequest.id}`)
      .update(updatedOfferRequest)
  }
}
