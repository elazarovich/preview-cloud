import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { saveSalesInvoice } from "./insert"
import { OfferDB } from "helpdor-types"

const { OFFERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const sendAllOffersToNetvisorAsSalesInvoices = async () => {
  const offersSnapshot = await admin
    .app()
    .database()
    .ref(`${OFFERS}`)
    .once("value")

  const offers: { [key: string]: OfferDB } = {}
  offersSnapshot.forEach(offerSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = offerSnapshot.val()
    if (offer.status === "completed" && !offer.netvisorSalesInvoiceId) {
      offers[offerSnapshot.key] = offer
    }
  })

  await Promise.all(
    Object.keys(offers).map(key =>
      saveSalesInvoice({
        offer: {
          id: key,
          ...offers[key],
        },
      }),
    ),
  )
}

export const pubsub = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllOffersToNetvisorAsSalesInvoices)

export const https = functions.https.onRequest(async (req, res) => {
  await sendAllOffersToNetvisorAsSalesInvoices()
  return res.send("done")
})
