import moment from "moment-timezone"
import * as admin from "firebase-admin"
import { makeNetvisorInsert } from "../request"
import { getUser } from "../../db/users"
import { getOfferRequest } from "../../db/offerRequests"
import { getCategory } from "../../db/categories"
import { REF_NAMES } from "../../db/RefNames"
import { CategoryDB, UserDB, OfferDB, OfferRequestDB } from "helpdor-types"
import {
  convertToDecimals,
  getActualOrderAmount,
  getActualOrderDuration,
} from "../helpers"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "salesinvoice.nv"

interface Props {
  offer: OfferDB & { id: string }
}

interface XMLProps extends Props {
  user: UserDB & Required<Pick<UserDB, "netvisorCustomerId">> & { id: string }
  offerRequest: OfferRequestDB &
    Required<Pick<OfferRequestDB, "netvisorSalesOrderId">> & { id: string }
  category: CategoryDB &
    Required<Pick<CategoryDB, "netvisorKey">> & { id: string }
}

const getInvoiceLinesXML = (props: XMLProps) =>
  `<invoicelines>
    <invoiceline>
      <salesinvoiceproductline>
        <productidentifier type="netvisor">${
          props.category.netvisorKey
        }</productidentifier>
        <productname>${props.category.name.fi}</productname>
        <productunitprice type="gross">${convertToDecimals(
          props.offer.pricePerHour,
        )}</productunitprice>
        <productvatpercentage vatcode="KOMY">22</productvatpercentage>
        <salesinvoiceproductlinequantity>${getActualOrderDuration(
          props.offer,
        )}</salesinvoiceproductlinequantity>
      </salesinvoiceproductline>
    </invoiceline>
  </invoicelines>`

const getSalesInvoiceXML = (props: XMLProps) => {
  return `<root>
    <salesinvoice>
      <salesinvoicedate>${moment().format("YYYY-MM-DD")}</salesinvoicedate>
      <salesinvoiceduedate format="ansi">${moment(
        props.offer.actualCompletedTime,
        "x",
      ).format("YYYY-MM-DD")}</salesinvoiceduedate>
      <salesinvoiceamount>${getActualOrderAmount(
        props.offer,
      )}</salesinvoiceamount>
      <invoicetype>invoice</invoicetype>
      <salesinvoicestatus type="netvisor">unsent</salesinvoicestatus>
      <invoicingcustomeridentifier type="netvisor">${
        props.user.netvisorCustomerId
      }</invoicingcustomeridentifier>
      <invoicingcustomername>${props.user.name}</invoicingcustomername>
      <invoicingcustomercountrycode type="ISO-3316">FI</invoicingcustomercountrycode>
      <deliveryaddressname>${props.user.name}</deliveryaddressname>
      <deliveryaddressline>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.streetAddress
          : props.offerRequest.addresses.to.streetAddress
      }</deliveryaddressline>
      <deliveryaddresspostnumber>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.postalCode
          : props.offerRequest.addresses.to.postalCode
      }</deliveryaddresspostnumber>
      <deliveryaddresstown>${
        props.offerRequest.addresses.type === "basic"
          ? props.offerRequest.addresses.main.city
          : props.offerRequest.addresses.to.city
      }</deliveryaddresstown>
      <deliveryaddresscountrycode type="ISO-3316">FI</deliveryaddresscountrycode>
      ${getInvoiceLinesXML(props)}
    </salesinvoice>
    </root>`
}

export const saveSalesInvoice = async (props: Props) => {
  if (props.offer.netvisorSalesInvoiceId) {
    console.log(
      `Offer (${props.offer.id}) already added to Netvisor with a id ${props.offer.netvisorSalesInvoiceId}`,
    )
    return
  } else if (props.offer.status !== "completed") {
    console.log(
      `Only completed offers can be added to Netvisor as invoices (${props.offer.id})`,
    )
    return
  }

  const database = admin.app().database()
  const user = await getUser(database, props.offer.buyerProfile)
  if (!user || user.netvisorCustomerId === undefined) {
    throw new Error("Invalid User")
  }
  const offerRequest = await getOfferRequest(database, props.offer.offerRequest)
  if (!offerRequest || offerRequest.netvisorSalesOrderId === undefined) {
    throw new Error("Invalid OfferRequest")
  }
  const category = await getCategory(database, offerRequest.category)
  if (!category || category.netvisorKey === undefined) {
    throw new Error("Invalid Category")
  }
  const payload = getSalesInvoiceXML({
    user: {
      netvisorCustomerId: user.netvisorCustomerId,
      ...user,
    },
    offerRequest: {
      netvisorSalesOrderId: offerRequest.netvisorSalesOrderId,
      ...offerRequest,
    },
    category: {
      netvisorKey: category.netvisorKey,
      ...category,
    },
    ...props,
  })

  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: `?method=edit&Id=${offerRequest.netvisorSalesOrderId}`,
    xml: payload,
  })

  if (responseData) {
    const updatedOffer: Partial<OfferDB> = {
      netvisorSalesInvoiceId: offerRequest.netvisorSalesOrderId,
    }
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.OFFERS}/${props.offer.id}`)
      .update(updatedOffer)
  }
}
