import * as admin from "firebase-admin"
import _ from "lodash"
import axios from "axios"
import { getHeaders, NETVISOR_URL } from "./requestHeaders"
import { parseString } from "xml2js"
import promisify from "util.promisify"
import { ErrorReporting } from "@google-cloud/error-reporting"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

type insertResource =
  | "product.nv"
  | "customer.nv"
  | "vendor.nv"
  | "salesinvoice.nv"
  | "purchaseorder.nv"
  | "salespayment.nv"
  | "purchaseinvoice.nv"
  | "getorder.nv"
  | "customerlist.nv"

const parseStringPromisified = promisify(parseString)

export const makeNetvisorInsert = async (props: {
  resource: insertResource
  options?: string
  xml?: string
}) => {
  const errors = new ErrorReporting()
  const url = `${NETVISOR_URL}${props.resource}${props.options || ""}`
  const response = await axios({
    method: "post",
    responseType: "document",
    url,
    data: props.xml,
    headers: getHeaders(url),
  })

  // tslint:disable-next-line:no-unsafe-any
  const responseData = await parseStringPromisified(response.data)
  const status = _.get(responseData, "Root.ResponseStatus[0].Status[0]")
  if (status === "OK") {
    return responseData as object
  } else {
    // tslint:disable-next-line:no-unsafe-any
    const error = new Error(
      `Request ${url} failed: ${_.get(
        responseData,
        "Root.ResponseStatus[0].Status[1]",
      )} ${props.xml}`,
    )
    // tslint:disable-next-line:no-unsafe-any
    errors.report(error)
    throw error
  }
}

type getResource = "customerlist.nv"

export const getFromNetvisor = async (props: {
  resource: getResource
  options?: string
}) => {
  const errors = new ErrorReporting()
  const url = `${NETVISOR_URL}${props.resource}${props.options || ""}`
  const response = await axios({
    method: "get",
    responseType: "document",
    url,
    data: {},
    headers: getHeaders(url),
  })

  // tslint:disable-next-line:no-unsafe-any
  const responseData = await parseStringPromisified(response.data)
  const status = _.get(responseData, "Root.ResponseStatus[0].Status[0]")
  if (status === "OK") {
    return responseData as object
  } else {
    // tslint:disable-next-line:no-unsafe-any
    const error = new Error(
      `Request ${url} failed: ${_.get(
        responseData,
        "Root.ResponseStatus[0].Status[1]",
      )}`,
    )
    // tslint:disable-next-line:no-unsafe-any
    errors.report(error)
    throw error
  }
}
