import * as admin from "firebase-admin"
// import { getTransaction } from "../../db/transactions"
// import { savePayment } from "./insertPayment"
// import { saveSalesOrder } from "./insertSalesOrder"
// import { saveCustomer } from "./insertCustomer"
import { saveServiceProvider } from "./serviceProvider/insert"
import { getUser, getSellerProfile } from "../db/users"
import { getOfferRequest } from "../db/offerRequests"
import { getOffer } from "../db/offers"
import { saveSalesOrder } from "./salesOrder/insert"
import { savePurchaseOrder } from "./purchaseOrder/insert"
import { saveSalesInvoice } from "./salesInvoice/insert"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const database = admin.app().database()

async function test() {
  // const customer = await getUser(database, "2owehXr8S8UqKdEbUPFaco4hzFT2")
  // await saveCustomer(customer, databaseUrl)

  const offerRequest = await getOfferRequest(database, "-Lvz3vxIzaftJBy7KuZF")
  await saveSalesOrder({
    pricePerHour: 29,
    duration: 2,
    offerRequest,
  })

  const serviceProvider = await getUser(
    database,
    "t53gzbWYm0RHoIBsDs4wZzlQ5BI3",
  )
  const sellerProfile = await getSellerProfile(
    database,
    "t53gzbWYm0RHoIBsDs4wZzlQ5BI3",
  )
  await saveServiceProvider({
    ...serviceProvider,
    companyEmail: sellerProfile.companyEmail,
    companyName: sellerProfile.companyName,
    companyVAT: sellerProfile.companyVAT,
    streetAddress: sellerProfile.streetAddress,
    postalCode: sellerProfile.postalCode,
    city: sellerProfile.city,
  })
  const offerToPurchaseOrder = await getOffer(database, "-Lwb1wYf8OXslNwkPLzp")
  console.log({ offerToPurchaseOrder })
  await savePurchaseOrder({
    offer: offerToPurchaseOrder,
  })

  // const transaction = await getTransaction(database, "-Lvz3x1sDsPSZmVroDR6")
  // console.log({ transaction })
  // await savePayment(
  //   {
  //     transaction,
  //   },
  //   DATABASES.stagingUrl,
  // )

  const offerToSalesInvoice = await getOffer(database, "-Lwb1wYf8OXslNwkPLzp")
  await saveSalesInvoice({
    offer: offerToSalesInvoice,
  })

  process.exit()
}
// tslint:disable-next-line:no-floating-promises
test()
