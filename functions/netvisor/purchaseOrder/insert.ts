import _ from "lodash"
import * as admin from "firebase-admin"
import { CategoryDB, UserDB, OfferRequestDB, OfferDB } from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"
import moment from "moment-timezone"
import { findOfferRequestDeadline } from "../../pubSub/index.func"
import { getUser } from "../../db/users"
import { getCategory } from "../../db/categories"
import { getOfferRequest } from "../../db/offerRequests"
import { convertToDecimals } from "../helpers"
import { serviceProvidersCut, defaultVAT } from "../configs"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "purchaseorder.nv"

interface Props {
  offer: OfferDB & { id: string }
}

interface XMLProps extends Props {
  seller: UserDB & Required<Pick<UserDB, "netvisorSellerId">> & { id: string }
  buyer: UserDB & Required<Pick<UserDB, "netvisorCustomerId">> & { id: string }
  offerRequest: OfferRequestDB &
    Required<Pick<OfferRequestDB, "netvisorSalesOrderId">> & { id: string }
  category: CategoryDB &
    Required<Pick<CategoryDB, "netvisorKey">> & { id: string }
}

export const getPurchaseOrderLinesXML = (props: XMLProps) => {
  const deadline = findOfferRequestDeadline(props.offerRequest)
  return `<purchaseorderlines>
    <purchaseorderproductline method="add">
      <productcode type="netvisor">${props.category.netvisorKey}</productcode>
      <orderedamount>${props.offer.original.purchaseDuration}</orderedamount>
      <unitprice>${convertToDecimals(
        (props.offer.original.pricePerHour * serviceProvidersCut) /
          (1 + defaultVAT),
      )}</unitprice>
      <vatpercent>${defaultVAT}</vatpercent>
      <deliverydate format="ansi">${moment(deadline, "x").format(
        "YYYY-MM-DD",
      )}</deliverydate>
    </purchaseorderproductline>
  </purchaseorderlines>`
}

export const getPurchaseOrderXML = (props: XMLProps) => {
  return `<root>
    <purchaseorder>
      <ordernumber>${Number(
        `${props.offerRequest.netvisorSalesOrderId}${Date.now()}`,
      )}</ordernumber>
      <orderstatus>approved</orderstatus>
      <orderdate format="ansi">${moment().format("YYYY-MM-DD")}</orderdate>
      <vendoridentifier type="netvisor">${
        props.seller.netvisorSellerId
      }</vendoridentifier>
      <purchaseorderdeliverydetails>
        <name>${props.buyer.name}</name>
        <address>${
          props.offerRequest.addresses.type === "basic"
            ? props.offerRequest.addresses.main.streetAddress
            : props.offerRequest.addresses.to.streetAddress
        }</address>
        <postnumber>${
          props.offerRequest.addresses.type === "basic"
            ? props.offerRequest.addresses.main.postalCode
            : props.offerRequest.addresses.to.postalCode
        }</postnumber>
        <city>${
          props.offerRequest.addresses.type === "basic"
            ? props.offerRequest.addresses.main.city
            : props.offerRequest.addresses.to.city
        }</city>
        <country type="ISO-3166">FI</country>
      </purchaseorderdeliverydetails>
      <privatecomment>Salesorder: ${
        props.offerRequest.netvisorSalesOrderId
      } Customer: ${props.buyer.netvisorCustomerId} (${
    props.buyer.name
  })</privatecomment>
      ${getPurchaseOrderLinesXML(props)}
    </purchaseorder>
    </root>`
}

export const savePurchaseOrder = async (props: Props) => {
  if (props.offer.netvisorPurchaseOrderId) {
    console.log(
      `OfferRequest (${props.offer.id}) already added to Netvisor with a id ${props.offer.netvisorPurchaseOrderId}`,
    )
    return
  }

  const database = admin.app().database()

  const seller = await getUser(database, props.offer.sellerProfile)
  if (!seller || seller.netvisorSellerId === undefined) {
    throw new Error("Invalid seller User")
  }

  const buyer = await getUser(database, props.offer.buyerProfile)
  if (!buyer || buyer.netvisorCustomerId === undefined) {
    throw new Error("Invalid buyer User")
  }

  const offerRequest = await getOfferRequest(database, props.offer.offerRequest)
  if (!offerRequest || offerRequest.netvisorSalesOrderId === undefined) {
    throw new Error("Invalid OfferRequest")
  }

  const category = await getCategory(database, offerRequest.category)
  if (!category || category.netvisorKey === undefined) {
    throw new Error("Invalid Category")
  }

  const payload = getPurchaseOrderXML({
    seller: {
      ...seller,
      netvisorSellerId: seller.netvisorSellerId,
    },
    buyer: {
      ...buyer,
      netvisorCustomerId: buyer.netvisorCustomerId,
    },
    offerRequest: {
      ...offerRequest,
      netvisorSalesOrderId: offerRequest.netvisorSalesOrderId,
    },
    category: {
      ...category,
      netvisorKey: category.netvisorKey,
    },
    ...props,
  })

  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: `?method=add`,
    xml: payload,
  })
  // tslint:disable-next-line:no-unsafe-any
  const netvisorKey: string | undefined = _.get(
    responseData,
    "Root.Replies[0].InsertedDataIdentifier[0]",
  )

  console.log(netvisorKey)
  if (responseData && netvisorKey) {
    const updatedOffer: Partial<OfferDB> = {
      netvisorPurchaseOrderId: netvisorKey,
    }
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.OFFERS}/${props.offer.id}`)
      .update(updatedOffer)
  }
}
