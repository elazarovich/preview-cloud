import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { savePurchaseOrder } from "./insert"
import { OfferDB } from "helpdor-types"

const { OFFERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const sendAllOffersToNetvisorAsPurchaseOrders = async () => {
  const offersSnapshot = await admin
    .app()
    .database()
    .ref(`${OFFERS}`)
    .once("value")

  const offers: { [key: string]: OfferDB } = {}
  offersSnapshot.forEach(offerSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = offerSnapshot.val()
    if (offer.status === "accepted" && !offer.netvisorPurchaseOrderId) {
      offers[offerSnapshot.key] = offer
    }
  })

  await Promise.all(
    Object.keys(offers).map(key =>
      savePurchaseOrder({
        offer: {
          id: key,
          ...offers[key],
        },
      }),
    ),
  )
}

export const pubsub = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllOffersToNetvisorAsPurchaseOrders)

export const https = functions.https.onRequest(async (req, res) => {
  await sendAllOffersToNetvisorAsPurchaseOrders()
  return res.send("done")
})
