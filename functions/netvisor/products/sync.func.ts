import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { syncProductsToNetvisor } from "./sync"

const { CATEGORIES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const categoryAdded = functions.database
  .ref(`/${CATEGORIES}/{pushId}`)
  .onWrite(syncProductsToNetvisor)

export const categoryPriceChanged = functions.database
  .ref(
    `/${CATEGORIES}/{pushId}/questions/fixed_offer/options/yes/questions/duration/price/default_value/{value}`,
  )
  .onWrite(syncProductsToNetvisor)
