import _ from "lodash"
import * as admin from "firebase-admin"
import {
  CategoryDB,
  CategoriesDB,
  CheckBoxQuestionDB,
  NumberInputQuestionDB,
} from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "product.nv"

const getPrice: (category: CategoryDB) => number = category => {
  const questions = category.questions
  const fixedOfferQuestionKey: string | undefined = Object.keys(questions).find(
    questionKey => questionKey === "fixed_offer",
  )
  if (fixedOfferQuestionKey) {
    const fixedOfferQuestion = questions[
      fixedOfferQuestionKey
    ] as CheckBoxQuestionDB
    const fixedPriceQuestion = fixedOfferQuestion.options.yes
      ? (fixedOfferQuestion.options.yes.questions
          .price as NumberInputQuestionDB)
      : undefined
    if (fixedPriceQuestion && fixedPriceQuestion.defaultValue) {
      return Number(fixedPriceQuestion.defaultValue)
    }
  }
  return 29
}

const getProductXML = (key: string, category: CategoryDB) =>
  `<root>
    <product>
      <productbaseinformation>
        <productcode>${key}</productcode>
        <productgroup>BASIC</productgroup>
        <name>${category.name.fi}</name>
        <unitprice type="gross">${getPrice(category)}</unitprice>
        <isactive>1</isactive>
        <issalesproduct>1</issalesproduct>
      </productbaseinformation>
    </product>
  </root>`

const createOrUpdateProductToNetvisor = async (
  key: string,
  category: CategoryDB,
) => {
  const payload = getProductXML(key, category)
  if (category.netvisorKey) {
    return makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=edit&id=${category.netvisorKey}`,
      xml: payload,
    })
  } else {
    const responseData = await makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=add`,
      xml: payload,
    })
    // tslint:disable-next-line:no-unsafe-any
    const netvisorKey: string | undefined = _.get(
      responseData,
      "Root.Replies[0].InsertedDataIdentifier[0]",
    )
    if (responseData && netvisorKey) {
      return netvisorKey
    }
    return undefined
  }
}

export const syncProductsToNetvisor = async () => {
  const database = admin.app().database()

  const categoriesSnapshots = await database
    .ref(`${REF_NAMES.CATEGORIES}`)
    .once("value")
  const categories: CategoriesDB = {}
  categoriesSnapshots.forEach(snapshot => {
    // tslint:disable-next-line:no-unsafe-any
    categories[snapshot.key] = snapshot.val()
  })
  return Promise.all(
    Object.keys(categories).map(async key => {
      const category = categories[key]
      const netvisorKey = await createOrUpdateProductToNetvisor(key, category)
      if (netvisorKey && !category.netvisorKey) {
        const updatedCategory: Partial<CategoryDB> = {
          netvisorKey: Number(netvisorKey),
        }
        await admin
          .app()
          .database()
          .ref(`${REF_NAMES.CATEGORIES}/${key}`)
          .update(updatedCategory)
      }
    }),
  )
}
