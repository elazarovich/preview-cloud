import _ from "lodash"
import * as admin from "firebase-admin"
import { TransactionDB, UserDB } from "helpdor-types"
import { getUser } from "../../db/users"
import { getOfferRequest } from "../../db/offerRequests"
import { getOffer } from "../../db/offers"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"
import { getSalesOrder } from "./getSalesOrder"
import moment from "moment-timezone"
import { convertToDecimals } from "../helpers"
import { defaultVAT } from "../configs"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "salespayment.nv"

interface Props {
  transaction: TransactionDB & { id: string }
}

export const getPaymentXML = (
  props: Props & {
    buyer: UserDB
    referenceNumber: string
  },
) =>
  `<root>
        <SalesPayment>
            <Sum Currency="EUR">${convertToDecimals(
              props.transaction.amount,
            )}</Sum>
            <PaymentDate>${moment(props.transaction.createdAt).format(
              "YYYY-MM-DD",
            )}</PaymentDate>
            <TargetIdentifier
              type="reference"
              targetType="order"
            >${props.referenceNumber}</TargetIdentifier>
            <SourceName>${props.buyer.name}</SourceName>
            <PaymentMethod Type="alternative">Stripe</PaymentMethod>
            <salespaymentvoucherlines>
              <voucherline>
                <LineSum type="gross">${convertToDecimals(
                  props.transaction.amount,
                )}</LineSum>
                <Description>Buyer:${props.buyer.name},Transaction:${
    props.transaction.id
  },OfferRequest:${props.transaction.offerRequestId}</Description>
                <AccountNumber>${props.buyer.netvisorCustomerId}</AccountNumber>
                <VatPercent VatCode="KOMY">${defaultVAT}</VatPercent>
              </voucherline>
            </salespaymentvoucherlines>
        </SalesPayment>
    </root>`

export const savePayment = async (props: Props) => {
  const transaction = props.transaction
  if (transaction.status !== "paid") {
    console.log(
      `Only paid transaction can be moved to Netvisor (${props.transaction.id})`,
    )
    return
  } else if (transaction.netvisorReferenceNumber) {
    console.log(
      `Transaction (${props.transaction.id}) already added to Netvisor with a reference ${props.transaction.netvisorReferenceNumber}`,
    )
    return
  }

  const database = admin.app().database()

  const buyer = await getUser(
    database,
    props.transaction.buyerInformation.buyerId,
  )
  if (!buyer || buyer.netvisorCustomerId === undefined) {
    throw new Error("Invalid User")
  }

  let targetNetvisorKey: string | undefined
  if (transaction.offerId) {
    const offer = await getOffer(database, transaction.offerId)
    if (offer && offer.netvisorPurchaseOrderId) {
      targetNetvisorKey = offer.netvisorPurchaseOrderId
    }
  } else if (transaction.offerRequestId) {
    const offerRequest = await getOfferRequest(
      database,
      transaction.offerRequestId,
    )
    if (offerRequest && offerRequest.netvisorSalesOrderId) {
      targetNetvisorKey = offerRequest.netvisorSalesOrderId
    }
  }

  if (!targetNetvisorKey) {
    throw new Error(
      "Adding payment to netvisor failed: Invalid Offer or OfferRequest for transaction",
    )
  }

  const netvisorOrder = await getSalesOrder(targetNetvisorKey) //
  const referenceNumber = netvisorOrder.SalesInvoiceReferencenumber[0]

  const payload = getPaymentXML({
    transaction,
    buyer,
    referenceNumber,
  })

  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: ``,
    xml: payload,
  })

  // tslint:disable-next-line:no-unsafe-any
  if (responseData) {
    const updatedTransaction: Partial<TransactionDB> = {
      netvisorReferenceNumber: Number(referenceNumber),
    }
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.TRANSACTIONS}/${transaction.id}`)
      .update(updatedTransaction)
  }
}
