import _ from "lodash"
import * as admin from "firebase-admin"
import { makeNetvisorInsert } from "../request"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

interface SalesOrder {
  SalesInvoiceNetvisorKey: string[]
  SalesInvoiceReferencenumber: string[]
}

const RESOURCE = "getorder.nv"

export const getSalesOrder = async (netvisorKey: string) => {
  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: `?netvisorkey=${netvisorKey}`,
  })
  // tslint:disable-next-line:no-unsafe-any
  const salesOrder: SalesOrder = _.get(responseData, "Root.SalesInvoice[0]")
  return salesOrder
}
