import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { savePayment } from "./insert"
import { TransactionDB } from "helpdor-types"

const { TRANSACTIONS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const sendAllPaymentsToNetvisor = async () => {
  const transactionsSnapshot = await admin
    .app()
    .database()
    .ref(`${TRANSACTIONS}`)
    .once("value")

  const transactions: { [key: string]: TransactionDB } = {}
  transactionsSnapshot.forEach(transactionSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const transaction: TransactionDB = transactionSnapshot.val()
    if (transaction.status === "paid" && !transaction.netvisorReferenceNumber) {
      transactions[transactionSnapshot.key] = transaction
    }
  })

  await Promise.all(
    Object.keys(transactions).map(key =>
      savePayment({
        transaction: {
          id: key,
          ...transactions[key],
        },
      }),
    ),
  )
}

export const pubsub = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllPaymentsToNetvisor)

export const https = functions.https.onRequest(async (req, res) => {
  await sendAllPaymentsToNetvisor()
  return res.send("done")
})
