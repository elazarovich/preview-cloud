import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { TransactionDB } from "helpdor-types"
import { savePayment } from "./insert"

const { TRANSACTIONS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const savePaymentToNetvisor = functions.database
  .ref(`/${TRANSACTIONS}/{pushId}`)
  .onWrite((change, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const transaction: TransactionDB = change.after.val()
    if (transaction.status === "paid" && !transaction.netvisorReferenceNumber) {
      return savePayment({
        transaction: {
          id: context.params.pushId as string,
          ...transaction,
        },
      })
    }
    return 0
  })
