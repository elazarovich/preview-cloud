import { OfferDB } from "helpdor-types"

export const convertToDecimals = (value: number | string) => {
  return typeof value === "string"
    ? parseFloat(value).toFixed(2)
    : value.toFixed(2)
}

export const getActualOrderDuration = (offer: OfferDB) => {
  return (
    (offer.original.actualDuration || offer.original.purchaseDuration) +
    (offer.extensions
      ? offer.extensions.reduce(
          (extensionDuration, extension) =>
            extensionDuration +
            (extension.actualDuration || extension.purchaseDuration),
          0,
        )
      : 0)
  )
}

export const getActualOrderAmount = (offer: OfferDB) => {
  return offer.original.pricePerHour * getActualOrderDuration(offer)
}
