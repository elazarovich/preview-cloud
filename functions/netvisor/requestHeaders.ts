import crypto from "crypto"

export const NETVISOR_URL = "https://isvapi.netvisor.fi/"
export const PARTNER_ID = "Hel_18575"
export const PARTNER_KEY = "0E92727893E731E5FA8EC1A7D8DA843B" // kumppaniavain
export const CUSTOMER_ID = "HD_88426_24469" // käyttäjätunnus
export const CUSTOMER_KEY = "DB0D48BA252B7568122EBFB9C48B9640" // käyttäjänavain
export const SENDER = "Test Requests"
export const LANG = "FI"
export const ORG_ID = "2844881-9"
export const ALGO = "SHA256"

export const getHeaders = (URL: string) => {
  const TIMESTAMP = new Date().toISOString()
  const TRANS_ID = `ID${Date.now()}${Math.round(Math.random() * 10000)}`
  // tslint:disable-next-line:max-line-length
  const hashSource = `${URL}&${SENDER}&${CUSTOMER_ID}&${TIMESTAMP}&${LANG}&${ORG_ID}&${TRANS_ID}&${CUSTOMER_KEY}&${PARTNER_KEY}`
  const hash = crypto
    .createHash("sha256")
    .update(hashSource)
    .digest("hex")
  return {
    "X-Netvisor-Authentication-Sender": SENDER,
    "X-Netvisor-Authentication-CustomerId": CUSTOMER_ID,
    "X-Netvisor-Authentication-PartnerId": PARTNER_ID,
    "X-Netvisor-Interface-Language": LANG,
    "X-Netvisor-Organisation-ID": ORG_ID,
    "X-Netvisor-Authentication-MACHashCalculationAlgorithm": ALGO,
    "X-Netvisor-Authentication-Timestamp": TIMESTAMP,
    "X-Netvisor-Authentication-TransactionId": TRANS_ID,
    "X-Netvisor-Authentication-MAC": hash,
    "Content-Type": "application/xml",
  }
}
