import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { saveServiceProvider } from "./insert"
import { SellerProfileDB } from "helpdor-types"
import { getUser } from "../../db/users"

const { SELLER_PROFILES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const saveSellerToNetvisor = functions.database
  .ref(`/${SELLER_PROFILES}/{pushId}`)
  .onCreate(async (snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const seller: SellerProfileDB = snap.val()
    const user = await getUser(
      admin.database(),
      context.params.pushId as string,
    )
    return saveServiceProvider({
      id: context.params.pushId as string,
      ...user,
      ...seller,
    })
  })

export const updateSellerInNetvisor = functions.database
  .ref(`/${SELLER_PROFILES}/{pushId}`)
  .onUpdate(async (change, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const seller: SellerProfileDB = change.after.val()
    const user = await getUser(
      admin.database(),
      context.params.pushId as string,
    )
    return saveServiceProvider({
      id: context.params.pushId as string,
      ...user,
      ...seller,
    })
  })
