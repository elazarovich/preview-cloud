import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { saveServiceProvider } from "./insert"
import { SellerProfileDB } from "helpdor-types"
import { getUser } from "../../db/users"

const { SELLER_PROFILES } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const sendAllSellersToNetvisor = async () => {
  const sellersSnapshot = await admin
    .app()
    .database()
    .ref(`${SELLER_PROFILES}`)
    .once("value")

  const sellers: { [key: string]: SellerProfileDB } = {}
  sellersSnapshot.forEach(sellerSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    sellers[sellerSnapshot.key] = sellerSnapshot.val()
  })

  await Promise.all(
    Object.keys(sellers).map(async key => {
      const user = await getUser(admin.database(), key)
      return saveServiceProvider({
        id: key,
        ...user,
        ...sellers[key],
      })
    }),
  )
}

export const pubsub = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllSellersToNetvisor)

export const https = functions.https.onRequest(async (req, res) => {
  await sendAllSellersToNetvisor()
  return res.send("done")
})
