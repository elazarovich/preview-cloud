import _ from "lodash"
import * as admin from "firebase-admin"
import { UserDB, SellerProfileDB } from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "vendor.nv"

type Seller = UserDB &
  Required<
    Pick<
      SellerProfileDB,
      | "companyName"
      | "companyEmail"
      | "companyVAT"
      | "streetAddress"
      | "city"
      | "postalCode"
    >
  > & { id: string }

export const getSellerXML = (seller: Seller) =>
  `
    <root>
      <vendor>
        <vendorbaseinformation>
          <code>${seller.id}</code>
          <name>${seller.companyName}</name>
          <address>${seller.streetAddress}</address>
          <postcode>${seller.postalCode}</postcode>
          <city>${seller.city}</city>
          <country type="ISO-3166">FI</country>
          <organizationid>${seller.companyVAT}</organizationid>
        </vendorbaseinformation>
        <vendorcontactdetails>
          <phonenumber>${seller.phone}</phonenumber>
          <email>${seller.companyEmail}</email>
          <contactpersonname>${seller.name}</contactpersonname>
          <contactpersonphonenumber>${seller.phone}</contactpersonphonenumber>
          <contactpersonemail>${seller.email}</contactpersonemail>
        </vendorcontactdetails>
      </vendor>
    </root>
  `

// TODO split companyAddress to separate fields
// <postcode></postcode>
// <city></city>

export const saveServiceProvider = async (user: Seller) => {
  const payload = getSellerXML(user)
  if (user.netvisorSellerId) {
    await makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=edit&netvisorkey=${user.netvisorSellerId}`,
      xml: payload,
    })
  } else {
    const responseData = await makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=add`,
      xml: payload,
    })
    // tslint:disable-next-line:no-unsafe-any
    const netvisorKey: string | undefined = _.get(
      responseData,
      "Root.Replies[0].InsertedDataIdentifier[0]",
    )
    if (responseData && netvisorKey) {
      const updatedUser: Partial<UserDB> = {
        netvisorSellerId: Number(netvisorKey),
      }
      await admin
        .app()
        .database()
        .ref(`${REF_NAMES.USERS}/${user.id}`)
        .update(updatedUser)
    }
  }
}
