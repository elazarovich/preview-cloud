import _ from "lodash"
import * as admin from "firebase-admin"
import {
  CategoryDB,
  UserDB,
  OfferRequestDB,
  OfferDB,
  SellerProfileDB,
} from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"
import moment from "moment-timezone"
import { getUser, getSellerProfile } from "../../db/users"
import { getCategory } from "../../db/categories"
import { getOfferRequest } from "../../db/offerRequests"
import {
  convertToDecimals,
  getActualOrderDuration,
  getActualOrderAmount,
} from "../helpers"
import { serviceProvidersCut, defaultVAT } from "../configs"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "purchaseinvoice.nv"

interface Props {
  offer: OfferDB & { id: string }
}

interface XMLProps extends Props {
  seller: UserDB &
    Required<Pick<UserDB, "netvisorSellerId">> & {
      id: string
    } & SellerProfileDB
  buyer: UserDB & Required<Pick<UserDB, "netvisorCustomerId">> & { id: string }
  offerRequest: OfferRequestDB &
    Required<Pick<OfferRequestDB, "netvisorSalesOrderId">> & { id: string }
  category: CategoryDB &
    Required<Pick<CategoryDB, "netvisorKey">> & { id: string }
}

export const getPurchaseInvoiceLinesXML = (props: XMLProps) => {
  return `<purchaseinvoicelines>
    <purchaseinvoiceline>
      <productcode>${props.category.netvisorKey}</productcode>
      <productname>${props.category.name}</productname>
      <deliveredamount>${getActualOrderDuration(props.offer)}</deliveredamount>
      <unitname>h</unitname>
      <unitprice>${convertToDecimals(
        props.offer.original.pricePerHour,
      )}</unitprice>
      <vatpercent>${defaultVAT}</vatpercent>
      <linesum type="brutto">${convertToDecimals(
        getActualOrderAmount(props.offer) * serviceProvidersCut,
      )}</linesum>
    </purchaseinvoiceline>
  </purchaseinvoicelines>`
}

export const getPurchaseInvoiceXML = (props: XMLProps) => {
  return `<root>
    <purchaseinvoice>
      <invoicenumber>${props.offer.netvisorPurchaseOrderId}</invoicenumber>
      <invoicedate format="ansi">${moment().format("YYYY-MM-DD")}</invoicedate>
      <invoicesource>finvoice</invoicesource>
      <duedate format="ansi">${moment()
        .add(15, "days")
        .format("YYYY-MM-DD")}</duedate>
      <purchaseinvoiceonround type="netvisor">open</purchaseinvoiceonround>
      <amount>${convertToDecimals(
        props.offer.original.pricePerHour *
          getActualOrderDuration(props.offer) *
          serviceProvidersCut,
      )}</amount>
      <accountnumber>${props.seller.iban}</accountnumber>
      <organizationidentifier>${
        props.seller.companyVAT
      }</organizationidentifier>
      <comment>SalesInvoice:
        ${props.offer.netvisorSalesInvoiceId}
        Customer: ${props.buyer.netvisorCustomerId} (${props.buyer.name})
      </comment>
      ${getPurchaseInvoiceLinesXML(props)}
    </purchaseinvoice>
    </root>`
}

export const savePurchaseInvoice = async (props: Props) => {
  if (props.offer.netvisorPurchaseInvoiceId) {
    console.log(
      `Offer (${props.offer.id}) already added to Netvisor with a id ${props.offer.netvisorPurchaseOrderId}`,
    )
    return
  }

  const database = admin.app().database()

  const sellerUser = await getUser(database, props.offer.sellerProfile)
  if (!sellerUser || sellerUser.netvisorSellerId === undefined) {
    throw new Error("Invalid seller User")
  }

  const sellerProfile = await getSellerProfile(
    database,
    props.offer.sellerProfile,
  )
  if (!sellerProfile) {
    throw new Error("Invalid seller User")
  }

  const buyerUser = await getUser(database, props.offer.buyerProfile)
  if (!buyerUser || buyerUser.netvisorCustomerId === undefined) {
    throw new Error("Invalid buyer User")
  }

  const offerRequest = await getOfferRequest(database, props.offer.offerRequest)
  if (!offerRequest || offerRequest.netvisorSalesOrderId === undefined) {
    throw new Error("Invalid OfferRequest")
  }

  const category = await getCategory(database, offerRequest.category)
  if (!category || category.netvisorKey === undefined) {
    throw new Error("Invalid Category")
  }

  const payload = getPurchaseInvoiceXML({
    seller: {
      ...sellerUser,
      ...sellerProfile,
      netvisorSellerId: sellerUser.netvisorSellerId,
    },
    buyer: {
      ...buyerUser,
      netvisorCustomerId: buyerUser.netvisorCustomerId,
    },
    offerRequest: {
      ...offerRequest,
      netvisorSalesOrderId: offerRequest.netvisorSalesOrderId,
    },
    category: {
      ...category,
      netvisorKey: category.netvisorKey,
    },
    ...props,
  })

  const responseData = await makeNetvisorInsert({
    resource: RESOURCE,
    options: `?method=edit&Id=${props.offer.netvisorPurchaseOrderId}`,
    xml: payload,
  })
  if (responseData) {
    const updatedOffer: Partial<OfferDB> = {
      netvisorPurchaseInvoiceId: props.offer.netvisorPurchaseOrderId,
    }
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.OFFERS}/${props.offer.id}`)
      .update(updatedOffer)
  }
}
