import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { OfferDB } from "helpdor-types"
import { savePurchaseInvoice } from "./insert"

const { OFFERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const saveOffer = functions.database
  .ref(`/${OFFERS}/{pushId}/status`)
  .onWrite(async (change, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const previousStatus: OfferDB["status"] = change.before.val()
    // tslint:disable-next-line:no-unsafe-any
    const currentStatus: OfferDB["status"] = change.after.val()

    const offerSnapshot = await change.after.ref.parent.once("value")
    // tslint:disable-next-line:no-unsafe-any
    const offer: OfferDB = offerSnapshot.val()
    if (
      currentStatus === "completed" &&
      previousStatus !== currentStatus &&
      !offer.netvisorPurchaseInvoiceId
    ) {
      return savePurchaseInvoice({
        offer: {
          ...offer,
          id: context.params.pushId as string,
        },
      })
    }
    return 0
  })
