import _ from "lodash"
import * as admin from "firebase-admin"
import { getFromNetvisor } from "../request"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "customerlist.nv"

export const getCustomerFromNetvisor = async (id: string) => {
  const responseData = await getFromNetvisor({
    resource: RESOURCE,
    options: `?keyword=${id}`,
  })
  // tslint:disable-next-line:no-unsafe-any
  const netvisorKey: string | undefined = _.get(
    responseData,
    "Root.Customerlist[0].Customer[0].Netvisorkey[0]",
  )
  return netvisorKey || undefined
}
