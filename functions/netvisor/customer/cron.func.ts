import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { saveCustomer } from "./insert"
import { UserDB } from "helpdor-types"

const { USERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const sendAllUsersToNetvisor = async () => {
  const usersSnapshot = await admin
    .app()
    .database()
    .ref(`${USERS}`)
    .once("value")

  const users: { [key: string]: UserDB } = {}
  usersSnapshot.forEach(userSnapshot => {
    // tslint:disable-next-line:no-unsafe-any
    const user: UserDB = userSnapshot.val()
    if (!user.netvisorCustomerId) {
      users[userSnapshot.key] = user
    }
  })

  await Promise.all(
    Object.keys(users).map(key =>
      saveCustomer({
        id: key,
        ...users[key],
      }),
    ),
  )
}

export const pubsub = functions.pubsub
  .schedule("every 1 hours")
  .onRun(sendAllUsersToNetvisor)

export const https = functions.https.onRequest(async (req, res) => {
  await sendAllUsersToNetvisor()
  return res.send("done")
})
