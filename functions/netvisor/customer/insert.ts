import _ from "lodash"
import * as admin from "firebase-admin"
import { UserDB } from "helpdor-types"
import { makeNetvisorInsert } from "../request"
import { REF_NAMES } from "../../db/RefNames"
import { getCustomerFromNetvisor } from "./find"

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

const RESOURCE = "customer.nv"

export const getExistingCustomerXML = (user: UserDB & { id: string }) =>
  `<root>
        <customer>
            <customerbaseinformation>
                <name>${user.name}</name>
                <phonenumber>${user.phone}</phonenumber>
                <email>${user.email}</email>
                <EmailInvoicingAddress>${
                  user.emailVerified ? user.email : ""
                }</EmailInvoicingAddress>
            </customerbaseinformation>
        </customer>
    </root>`

export const getNewCustomerXML = (user: UserDB & { id: string }) =>
  `<root>
        <customer>
            <customerbaseinformation>
                <internalidentifier>${user.id}</internalidentifier>
                <name>${user.name}</name>
                <phonenumber>${user.phone}</phonenumber>
                <email>${user.email}</email>
                <EmailInvoicingAddress>${
                  user.emailVerified ? user.email : ""
                }</EmailInvoicingAddress>
            </customerbaseinformation>
        </customer>
    </root>`

export const saveCustomer = async (user: UserDB & { id: string }) => {
  let existingNetvisorCustomerId: number | undefined = user.netvisorCustomerId
  // if user.netvisorCustomerId doesn't exist, try find user in Netvisor
  if (!existingNetvisorCustomerId) {
    const key = await getCustomerFromNetvisor(user.id)
    if (key) {
      existingNetvisorCustomerId = parseInt(key, 10)
    }
  }

  let netvisorCustomerId: number | undefined
  if (existingNetvisorCustomerId) {
    // if user is in Netvisor -> update
    await makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=edit&id=${existingNetvisorCustomerId}`,
      xml: getExistingCustomerXML(user),
    })
    netvisorCustomerId = existingNetvisorCustomerId
  } else {
    // if user is not in Netvisor -> add
    const responseData = await makeNetvisorInsert({
      resource: RESOURCE,
      options: `?method=add`,
      xml: getNewCustomerXML(user),
    })

    // tslint:disable-next-line:no-unsafe-any
    const key: string | undefined = _.get(
      responseData,
      "Root.Replies[0].InsertedDataIdentifier[0]",
    )
    if (key) {
      netvisorCustomerId = parseInt(key, 10)
    }
  }

  // if netvisor request was successful
  // always make sure netvisorKey is in our DB
  if (netvisorCustomerId) {
    await admin
      .app()
      .database()
      .ref(`${REF_NAMES.USERS}/${user.id}`)
      .update({
        netvisorCustomerId,
      } as Pick<UserDB, "netvisorCustomerId">)
  } else {
    throw new Error(`Failed to create/update user ${user.id} to Netvisor`)
  }
}
