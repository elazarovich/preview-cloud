import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import { REF_NAMES } from "../../db/RefNames"
import { saveCustomer } from "./insert"
import { UserDB } from "helpdor-types"

const { USERS } = REF_NAMES

try {
  admin.initializeApp()
} catch (e) {
  // The admin SDK can only be initialized once.
  // If it has been done in another file we need to handle the error
}

export const saveNewUserToNetvisor = functions.database
  .ref(`/${USERS}/{pushId}`)
  .onCreate((snap, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const user: UserDB = snap.val()
    return saveCustomer({
      id: context.params.pushId as string,
      ...user,
    })
  })

export const saveExistingUserToNetvisor = functions.database
  .ref(`/${USERS}/{pushId}`)
  .onUpdate((change, context) => {
    // tslint:disable-next-line:no-unsafe-any
    const before: UserDB = change.before.val()
    // tslint:disable-next-line:no-unsafe-any
    const after: UserDB = change.after.val()

    if (
      before.name !== after.name ||
      before.phone !== after.phone ||
      before.email !== after.email ||
      before.emailVerified !== after.emailVerified
    ) {
      return saveCustomer({
        id: context.params.pushId as string,
        ...after,
      })
    }
    return 0
  })
